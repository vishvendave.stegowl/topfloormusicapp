import React from 'react'

function pr() {
  return (
    <div>
      
    </div>
  )
}

export default pr

// import React, { useEffect, useState } from 'react';
// import { useParams } from 'react-router-dom';
// import axios from 'axios';
// import Navbar from './Navbar';
// import Banner from './Banner';
// import Footer from './Footer';
// import { VITE_API_BASE_URL } from '../api/env';

// export default function Categorymusic() {
//     const { id } = useParams();
//     const [music, setMusic] = useState([]);

//     useEffect(() => {
//         const fetchData = async () => {
//             try {
//                 const response = await axios.post(`${VITE_API_BASE_URL}/songs`, {
//                     user_id: localStorage.getItem('userId'),
//                     songcategory_id: id
//                 });
//                 console.log(response.data);
//                 setMusic(response.data.data);
//             } catch (error) {
//                 console.error('Error fetching song data:', error);
//             }
//         };
//         fetchData();
//     }, [id]);


//     const handleAddFavorite = async (song_id) => {
//       try {
//         const userId = localStorage.getItem('userId');
//         const token = localStorage.getItem('token');
  
//         if (!userId || !token) {
//           console.error('User ID or token not found in local storage');
//           return;
//         }
  
//         await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
//           user_id: userId,
//           song_id: song_id,
//         }, {
//           headers: {
//             'Content-Type': 'application/json',
//             'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
//             'Authorization': `Bearer ${token}`,
//           },
//         });
//         console.log('Song added to favorites');
  
//         // Optionally, you can update the UI or show a success message here
//       } catch (error) {
//         console.error('Error adding to favorites:', error);
//         // Handle error or show error message
//       }
//     };


//     const handleAddToPlaylist = async (songId) => {
//       try {
//         const newPlaylistName = prompt('Enter the name of the playlist:');
//         if (!newPlaylistName) return; // Cancelled or empty input
        
//         const userId = localStorage.getItem('userId');
//         const token = localStorage.getItem('token');
  
//         if (!userId || !token) {
//           console.error('User ID or token not found in local storage');
//           return;
//         }
  
//         const playlistId = await createOrGetPlaylist(newPlaylistName);
  
//         await axios.post(`${VITE_API_BASE_URL}/playlistsong/add`, {
//           user_id: userId,
//           song_id: songId,
//           playlist_id: playlistId,
//         }, {
//           headers: {
//             'Content-Type': 'application/json',
//             'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
//             'Authorization': `Bearer ${token}`,
//           },
//         });
//         console.log('Song added to playlist successfully');
//       } catch (error) {
//         console.error('Error adding song to playlist:', error);
//       }
//     };
  
//     const createOrGetPlaylist = async (playlistName) => {
//       try {
//         const userId = localStorage.getItem('userId');
//         const response = await axios.post(`${VITE_API_BASE_URL}/playlist/add`, {
//           user_id: userId,
//           name: playlistName,
//         }, {
//           headers: {
//             'Content-Type': 'application/json',
//             'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
//           },
//         });
//         if (response.status === 200) {
//           return response.data.playlist_id;
//         }
//       } catch (error) {
//         console.error('Error creating or getting playlist:', error);
//         throw error;
//       }
//     };

//     return (
//         <div>
//             <Navbar />
//             <Banner />
//             <div className='container'>
//                 <h1>Music</h1>
//                 <div className="row">
//                     {music.map((song, index) => (
//                         <div className="col-md-4 mb-2" key={index}>
//                             <div className="card">
//                                 <img
//                                     src={song.song_image}
//                                     className="card-img-top"
//                                     alt="Song Cover"
//                                     height={200}
//                                     width={150}
                                    
//                                     style={{ cursor: 'pointer' }}
//                                 />
//                                 <div className="card-body">
//                                     <h5 className="card-title">{song.song_name}</h5>
//                                     <p className="card-text">{song.song_artist}</p>
//                                     <div>
//                                         <button
//                                             className="btn btn-success me-2"
//                                             onClick={() => handleAddToPlaylist(song.song_id)}
//                                         >
//                                             Add to Playlist
//                                         </button>
//                                         <button
//                                             className="btn btn-success my-2"
//                                             onClick={() => handleAddFavorite(song.song_id)}
//                                         >
//                                             Add to Favorites
//                                         </button>
//                                         {/* <button onClick={() => handleAddFavorite(song.song_id)}> Add to favorite</button> */}
//                                     </div>
                                    
//                                 </div>
//                             </div>
//                         </div>
//                     ))}
//                 </div>
//             </div>
//             <Banner />
//             <Footer />
//         </div>
//     );
// }