import axios from 'axios';

const instance=axios.create({
    baseURL:'https://topfloormusicapp.com/auth/api',
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',

        // 'Content-Type': 'application/json',
        // 'X-CSRFToken': null, 
    },
})

const VITE_API_BASE_URL = 'https://topfloormusicapp.com/auth/api'

const fetchData = async (endpoint) => {
    const userId = localStorage.getItem('userId');
    const token = localStorage.getItem('token');
    try {
        const response = await instance.get(endpoint);
        console.log('✌️response --->', response.data);
        return response.data;

        // const bannerData = bannerResponse.data.homeslider_data;
        // setBanners(bannerData);
    } 
    catch (error) {
        console.error('Error fetching data:', error);
    }
};


const postData=async (endpoint)=>{
  const userId = localStorage.getItem('userId');
  const token = localStorage.getItem('token');
  try{
    if (!userId || !token) {
      console.error('User ID or token not found in local storage');
      return;
    }
    const response=await instance.post(endpoint,{ user_id: userId})
    return response.data
  }
  catch(error){
    console.error('Error fetching data:', error);
  }
}

// const fetchFavorites = async () => {
//   try {
//     const userId = localStorage.getItem('userId');
//     const token = localStorage.getItem('token');

//     if (!userId || !token) {
//       console.error('User ID or token not found in local storage');
//       return;
//     }

//     const response = await axios.post(`${VITE_API_BASE_URL}/favourites`, { user_id: userId }, {
//       headers: {
//         'Content-Type': 'application/json',
//         'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
//         'Authorization': `Bearer ${token}`,
//       }
//     });

//     if (response.data && Array.isArray(response.data.data)) {
//       setFavoriteItems(response.data.data);
//     } else {
//       console.error('Array of favorite items not found in response:', response.data);
//     }
//   } catch (error) {
//     console.error('Error fetching favorite items:', error);
//   }
// };

// const fetchPlaylists = async () => {
//     const userId = localStorage.getItem('userId');
//     const token = localStorage.getItem('token');
//     try {
//       const response = await axios.post(`${VITE_API_BASE_URL}/playlist`, {
//         user_id: userId,
//       }, {
//         headers: {
//           'Content-Type': 'application/json',
//           'Authorization': `Bearer ${token}`,
//           // 'X-CSRFToken': 'k', // You can include the CSRF token if necessary
//         },
//       });
//       return response
//     //   if (response.status === 200) {
//     //     setExistingPlaylists(response.data.data); // Assuming playlists are in the data field
//     //   } else {
//     //     console.error('Error fetching playlists');
//     //   }
//     } catch (error) {
//       console.error('Error fetching playlists');
//     }
//   };
  

// const fetchData2= async () => {
//     try {
//         const imageDataResponse = await axios.get(`${VITE_API_BASE_URL}/cms`);

//         const imageData = imageDataResponse.data.songcategory_data;
//         setTrendings(imageData);
//     } catch (error) {
//         console.error('Error fetching image data:', error);
//     }
// };

  const services={
    getHomeData:()=>fetchData('/cms'),
    // getTrending:()=>fetchData(),
    // fetchPlaylists,
    fetchPlaylists:()=>postData('/playlist'),
    fetchFavorites:()=>postData('/favourites'),
    fetchMusicData:()=>fetchData('/menu'),
    fetchRadioData:()=>fetchData('/liveradio'),
    fetchEventData:()=>fetchData('/liveevent'),
    fetchLiveStreamData:()=>fetchData('/livestream'),
    fetchAdditionalData:()=>postData('/livevideo'),
    fetchRadioApData:()=>fetchData('/program'),
    fetchAllCategoryData:(id)=>fetchData(`/songcategory/${id}`)
  }

  export default services;