import React, { useEffect } from 'react'
import PlaylistsPage from '../components/PlaylistsPage'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import Banner from '../components/Banner'
import { useNavigate } from 'react-router-dom'

function MyPlayList() {

  const navigate = useNavigate();
  useEffect(() => {

    window.scrollTo(0, 0);

  }, []);
  useEffect(() => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');
      
      if (!userId || !token) {
        navigate('/');
      }
    } catch (error) {
      console.error('Error accessing local storage:', error);
    }
  }, []);
  
  return (
    <div>
      {/* <FetchPlayList/> */}
        <PlaylistsPage/>

    </div>
  )
}

export default MyPlayList
