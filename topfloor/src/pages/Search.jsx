import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../App.css'
import MusicPlayer from '../components/MusicPlayer';
import { VITE_API_BASE_URL } from '../api/env';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedSong, setSongs } from '../redux/MusicSlice';
import ToastNotification from '../components/ToastNotification';
import { useNavigate } from 'react-router-dom';
import searchAPI from '../services/homeServices'
import playBtn from '../assets/images/play-icon2.png'


function SearchForm() {
  const [searchKey, setSearchKey] = useState('');
  const [searchResult, setSearchResult] = useState([]);
  const [loading, setLoading] = useState(false);
  const [notification, setNotification] = useState(null);
  const [music, setMusic] = useState([]);
  const userId = localStorage.getItem('userId');
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [existingPlaylists, setExistingPlaylists] = useState([]);
  const [showPlaylists, setShowPlaylists] = useState(""); // State for the selected song ID
  const [newPlaylistName, setNewPlaylistName] = useState("");
  useEffect(() => {

    window.scrollTo(0, 0);

  }, []);
  useEffect(() => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        navigate('/');
      }
    } catch (error) {
      console.error('Error accessing local storage:', error);
    }
  }, []);

  useEffect(() => {
    searchAPI.fetchPlaylists().then((res)=>{
      setExistingPlaylists(res.data); // Assuming playlists are in the data field
      console.log(setExistingPlaylists)
    }).catch((error)=>{
      console.error('Error fetching playlists',error);
    })
    // const fetchPlaylists = async () => {
    //   const userId = localStorage.getItem('userId');
    //   const token = localStorage.getItem('token');
    //   try {
    //     const response = await axios.post(`${VITE_API_BASE_URL}/plylist`, {
    //       user_id: userId,
    //     }, {
    //       headers: {
    //         'Content-Type': 'application/json',
    //         'Authorization': `Bearer ${token}`,
    //       },
    //     });
    //     if (response.status === 200) {
    //       setExistingPlaylists(response.data.data); // Assuming playlists are in the data field
    //       console.log(setExistingPlaylists)
    //     } else {
    //       console.error('Error fetching playlists');
    //     }
    //   } catch (error) {
    //     console.error('Error fetching playlists');
    //   }
    // };
    // fetchPlaylists();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    try {
      const response = await axios.post(
        `${VITE_API_BASE_URL}/songs_search/${searchKey}`,
        { user_id: userId },
        {
          headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRFToken': 'tzng'
          }
        }
      );

      setSearchResult(response.data);
    } catch (error) {
      console.error('Error searching for songs:', error);
    } finally {
      setLoading(false);
    }
  };

  const handleImageClick = (songId) => {
    dispatch(setSelectedSong(songId));
    dispatch(setSongs(searchResult.data)); // Assuming searchResult.data is an array of songs
  };


  const handleAddFavorite = async (song_id) => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        console.error('User ID or token not found in local storage');
        return;
      }

      const response = await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
        user_id: userId,
        song_id: song_id,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
          'Authorization': `Bearer ${token}`,
        },
      });

      const isAddedToFavorites = response.data.favourites_status;
      const updatedMusic = music.map(song =>
        song.song_id === song_id ? { ...song, favorite: isAddedToFavorites } : song
      );
      setMusic(updatedMusic);

      const notificationMessage = isAddedToFavorites ? 'Song Added to favorites' : 'Song Removed from favorites';
      setNotification(notificationMessage);
      setTimeout(() => setNotification(null), 3000);
    } catch (error) {
      console.error('Error adding to favorites:', error);
    }
  };

  // const handleAddFavorite = async (song_id) => {
  //   try {
  //     const userId = localStorage.getItem('userId');
  //     const token = localStorage.getItem('token');

  //     if (!userId || !token) {
  //       console.error('User ID or token not found in local storage');
  //       return;
  //     }
  //     console.log(existingPlaylists)
  //     const isFavorite = existingPlaylists.some(playlist => playlist.song_id === song_id);
  //     const action = isFavorite ? 'remove' : 'add';

  //     await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
  //       user_id: userId,
  //       song_id: song_id,
  //     }, {
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
  //         'Authorization': `Bearer ${token}`,
  //       },
  //     });

  //     console.log(`Song ${action === 'add' ? 'added to' : 'removed from'} favorites`);
  //     setNotification(`Song ${action === 'add' ? 'Added To' : 'Removed From'} Favorite`); 
  //     setTimeout(() => setNotification(null), 3000);
  //   } catch (error) {
  //     console.error('Error adding to/removing from favorites:', error);
  //   }
  // };


  const handleAddToPlaylist = (songId) => {
    setShowPlaylists(songId); // Set the selected song ID
  };

  const addToPlaylist = async (songId, playlistId) => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        console.error('User ID or token not found in local storage');
        return;
      }

      await axios.post(`${VITE_API_BASE_URL}/playlistsong/add`, {
        user_id: userId,
        song_id: songId,
        playlist_id: playlistId,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
          'Authorization': `Bearer ${token}`,
        },
      });
      console.log('Song added to playlist successfully');
      setNotification('Song Added to Playlist SuccessFully');
      setTimeout(() => setNotification(null), 3000);
      setShowPlaylists("");
    } catch (error) {
      console.error('Error adding song to playlist:', error);
    }
  };

  const createNewPlaylist = async (songId, playlistName) => {
    try {
      const userId = localStorage.getItem('userId');
      const response = await axios.post(`${VITE_API_BASE_URL}/playlist/add`, {
        user_id: userId,
        name: playlistName,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
        },
      });
      if (response.status === 200) {
        // Add the new playlist to existingPlaylists state
        setExistingPlaylists(prevPlaylists => [...prevPlaylists, response.data]);
        // Add the song to the newly created playlist
        await addToPlaylist(songId, response.data.playlist_id);
        setNewPlaylistName(""); // Clear the input field
        setNotification('Playlist Added Successfully and Song Added to Playlist');
        setTimeout(() => setNotification(null), 3000);
        setShowPlaylists("");
      }
    } catch (error) {
      console.error('Error creating or getting playlist:', error);
    }
  };

  return (
    <div className='container'>
      {notification && (
        <ToastNotification message={notification} duration={3000} />
      )}

<div className={showPlaylists ? "playlist-section" : ""}>

{searchResult && searchResult.data && searchResult.data.map((song, index) => (
  <div key={index}>
    {showPlaylists === song.song_id && (
      <div className='modal-content'>
        <button className="btn btn-danger event-cross-btn" onClick={() => setShowPlaylists("")}>X</button>
        <div class="modal-icon"><i class="fas fa-music" aria-hidden="true"></i></div>

        <form>
          <div className="from-group">
            <input type="text" value={newPlaylistName} onChange={(e) => setNewPlaylistName(e.target.value)} placeholder="Enter new playlist name" className="from-control white" />
            <div className="form-submit">
              <button onClick={() => createNewPlaylist(song.song_id, newPlaylistName)} type="submit" className=" submit-playlist"> Submit </button>
            </div>
          </div>
        </form>
        <div class="playlist-box mt-4">
          <ul>
            {existingPlaylists.map(playlist => (

              <li key={playlist.playlist_id}>
                <a href="" onClick='{addToPlaylist(song.song_id, playlist.playlist_id)}' class="playlist-box-text">{playlist.name}</a>
              </li>

            ))}
          </ul>
        </div>
      </div>
    )}
  </div>
))}

</div>

      {/* <div className={showPlaylists ? "playlist-section" : ""}>
        <div className="card">
          <div className="card-body">
            {searchResult && searchResult.data && searchResult.data.map((song, index) => (
              <div key={index}>
                {showPlaylists === song.song_id && (
                  <div>
                    <button className="btn btn-sm btn-secondary mb-2" onClick={() => setShowPlaylists("")}>Hide</button>
                    <h2>Choose Playlist:</h2>
                    {existingPlaylists.map(playlist => (
                      <div key={playlist.playlist_id}>
                        <button onClick={() => addToPlaylist(song.song_id, playlist.playlist_id)}>{playlist.name}</button>
                      </div>
                    ))}
                    <input
                      type="text"
                      value={newPlaylistName}
                      onChange={(e) => setNewPlaylistName(e.target.value)}
                      placeholder="Enter new playlist name"
                    />
                    <button onClick={() => createNewPlaylist(song.song_id, newPlaylistName)}>Create Playlist</button>
                  </div>
                )}
              </div>
            ))}
          </div>
        </div>
      </div> */}

      <div className='container mt-3'>
        <form onSubmit={handleSubmit}>
          <div className='main-heading pb-5'>
            <h1 className='title-section pt-0 pb-2 red '>Search Song</h1>
          </div>
          <div className='d-flex'>
            <input type="text" className='search-input' value={searchKey} onChange={(e) => setSearchKey(e.target.value)} />&nbsp;
            <button type="submit" className='search-btn' disabled={loading}>Search</button>
          </div>

        </form>
      </div>

      {loading && <p>Loading...</p>}

      {/* {searchResult && searchResult.data && (
        <div>
          <h2>Search Result:</h2>
          {searchResult.data.map((song, index) => (
            <div key={song.song_id}>
              <img src={song.song_image} alt={song.song_name} style={{ cursor: 'pointer' }} height={150} width={150} onClick={() => handleImageClick(song.song_id)} />
              <p>{song.song_name} - {song.song_artist}</p>
              <button className="btn btn-success me-2" onClick={() => handleAddToPlaylist(song.song_id)}>Add to Playlist</button>
              <button className="btn btn-success" onClick={() => handleAddFavorite(song.song_id)}>Add to Favorites</button>
            </div>
          ))}
        </div>
      )} */}



<div className="container">
        {searchResult && searchResult.data && (
          <div>
            <h2>Search Result:</h2>
            <div className="card-container">
              {searchResult.data.map((song, index) => (
            <div class="card">
            <div class="card-image">
              <img
              src={song.song_image} className="card-img-top" alt={song.song_name}
            
                height={200}
                width={150}
                onClick={() => handleImageClick(song.song_id)}
                style={{ cursor: 'pointer' }}
              />
              <div class="card-overlay"  onClick={() => handleImageClick(song.song_id)}>
              <div className='plyBtn'>
                  <span><img src={playBtn} alt="" /></span>
                </div>
                <div class="more-options">
                  <div class="dots">⁞</div>
                  <div class="dropdown">

                    <button className='btn-drop' onClick={() => handleAddToPlaylist(song.song_id)}>Add to Playlist</button>

                    <button className='btn-drop' onClick={() => handleAddFavorite(song.song_id)}>Add to Favorites</button>
                  </div>
                </div>
                
              </div>
              
            </div>
            <div class="card-content">
              <p className="card-title">{song.song_name}</p>
              <p className="card-artist">{song.song_artist}</p>

            </div>
          </div>
              ))}
            </div>
          </div>
        )}
      </div>

      {/* <div className="container">
        {searchResult && searchResult.data && (
          <div>
            <h2>Search Result:</h2>
            <div className="row">
              {searchResult.data.map((song, index) => (
                <div className="col-md-4 mb-4" key={song.song_id}>
                  <div className="card">
                    <img src={song.song_image} className="card-img-top" alt={song.song_name} style={{ cursor: 'pointer' }} height={150} width={150} onClick={() => handleImageClick(song.song_id)} />
                    <div className="card-body">
                      <h5 className="card-title">{song.song_name}</h5>
                      <p className="card-text">{song.song_artist}</p>
                      <button className="btn btn-success me-2 my-2" onClick={() => handleAddToPlaylist(song.song_id)}>Add to Playlist</button>
                      <button className="btn btn-success" onClick={() => handleAddFavorite(song.song_id)}>Add to Favorites</button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
      </div> */}

    </div>
  );
}

export default SearchForm;

