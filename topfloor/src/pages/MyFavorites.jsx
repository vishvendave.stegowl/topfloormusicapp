import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Navbar from '../components/Navbar';
import Banner from '../components/Banner';
import { VITE_API_BASE_URL } from '../api/env';
import Footer from '../components/Footer';
import { useNavigate } from 'react-router-dom';
import ToastNotification from '../components/ToastNotification';
import { setSongs } from '../redux/MusicSlice';
import { setSelectedSong } from '../redux/MusicSlice';
import { useDispatch, useSelector } from 'react-redux';
import favApi from '../services/homeServices'

const MyFavorites = () => {
  const [favoriteItems, setFavoriteItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const navigate = useNavigate();
  const [notification, setNotification] = useState(null);
  const dispatch = useDispatch();
  useEffect(() => {

    window.scrollTo(0, 0);

  }, []);

  useEffect(() => {
    favApi.fetchFavorites().then((res)=>{
      if (res && Array.isArray(res.data)) {
        setFavoriteItems(res.data);
      } else {
        console.error('Array of favorite items not found in response:', res);
        setError('Array of favorite items not found in response');
      }
    }).finally(()=> {
      setLoading(false);
    })

    // const fetchFavorites = async () => {
    //   try {
    //     const userId = localStorage.getItem('userId');
    //     const token = localStorage.getItem('token');

    //     if (!userId || !token) {
    //       navigate('/');
    //     }

    //     const response = await axios.post(`${VITE_API_BASE_URL}/favourites`, { user_id: userId }, {
    //       headers: {
    //         'Content-Type': 'application/json',
    //         'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
    //         'Authorization': `Bearer ${token}`,
    //       }
    //     });

    //     if (response.data && Array.isArray(response.data.data)) {
    //       setFavoriteItems(response.data.data);
    //     } else {
    //       console.error('Array of favorite items not found in response:', response.data);
    //       setError('Array of favorite items not found in response');
    //     }
    //   } catch (error) {
    //     console.error('Error fetching favorite items:', error);
    //     setError('Error fetching favorite items');
    //   } finally {
    //     setLoading(false);
    //   }
    // };

    // fetchFavorites();
  }, []);

  const handleRemoveFavorite = async (songId) => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        throw new Error('User ID or token not found in local storage');
      }
      const confirmation = window.confirm('Are you sure you want to remove from Favorite ?');
      if (!confirmation) {
        return; // If the user cancels, do nothing
      }

      const response = await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
        user_id: userId,
        song_id: songId
      }, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        }
      });

      if (response.status === 200) {
        // Remove the item from the local favoriteItems state
        setFavoriteItems(prevItems => prevItems.filter(item => item.song_id !== songId));
        setNotification('Song removed from favorites');
        setTimeout(() => setNotification(null), 3000);
      } else {
        console.error('Failed to remove favorite item:', response.data);
      }
    } catch (error) {
      console.error('Error removing favorite item:', error);
    }
  };

  const handleImageClick = (index) => {
    dispatch(setSelectedSong(index.song_id));
    console.log(setSelectedSong(index.song_id));
    dispatch(setSongs(favoriteItems))
  };

  return (
    <>
      {notification && (
        <ToastNotification message={notification} duration={3000} />
      )}

      {loading ? (
        <div className="d-flex justify-content-center">
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      ) : (
        <>
          <div className="container" >
            <div>
              <h5 className='fav-heading'>My Favorite Items</h5>
              <br />
              {favoriteItems.length > 0 ? (
                <div className="row">
                  {favoriteItems.map((item, index) => (
                    <div className="col-md-2 mb-2" key={index}>
                      <div class="card" >
                        <div class="card-image">
                          <img
                            src={item.song_image}
                            alt="Album Art"
                            height={200}
                            width={150}
                            onClick={() => handleImageClick(song)}
                            style={{ cursor: 'pointer' }}
                          />
                          <div class="card-overlay" onClick={() => handleImageClick(item)}>
                            <div class="more-options">
                              <div class="dots">⁞</div>
                              <div class="dropdown">

                                <button className='btn-drop' onClick={() => handleRemoveFavorite(item.song_id)}>Remove</button>

                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="card-content">
                          <p className="card-title">{item.song_name}</p>
                          <p className="card-artist">{item.song_artist}</p>

                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              ) : (
                <div className="empty">
                  <div className="inner-empty">
                    <p>❤</p>
                   <h2>No favorite items found</h2>
                  </div>
                  
                </div>
                
              )}
            </div>
          </div>

          {/* <div>
            <h1>My Favorite Items</h1> <br />
            {favoriteItems.length > 0 ? (
              <div>
                <br />
                {favoriteItems.map((item, index) => (
                  <div key={index}>
                    <img src={item.song_image} alt="Logo" height={200} width={150} onClick={() => handleImageClick(item)} style={{cursor:'pointer'}}/>
                    <p>{item.song_name}</p>
                    <p>{item.song_artist}</p>
                    <button onClick={() => handleRemoveFavorite(item.song_id)}>Remove</button> 
                  </div>
                ))}
              </div>
            ) : (
              <p>No favorite items found</p>
            )}
          </div> */}
        </>
      )}
    </>
  );
};

export default MyFavorites;
