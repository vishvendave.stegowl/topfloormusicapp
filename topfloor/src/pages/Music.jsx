import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { VITE_API_BASE_URL } from '../api/env';
import '../App.css'
import musicApi from '../services/homeServices'

const Music = () => {
    const [music, setMusic] = useState([]);
    const navigate = useNavigate();
    useEffect(() => {

        window.scrollTo(0, 0);
    
      }, []);
    useEffect(() => {
        musicApi.fetchMusicData().then((res)=>{
            const musicData=res.data;
            setMusic(musicData)
        })
        // const fetchData = async () => {
        //     try {
        //         const userId = localStorage.getItem('userId');
        //         const token = localStorage.getItem('token');
                
        //         if (!userId || !token) {
        //         navigate('/');
        //         }
                
        //         const musicResponse = await axios.get(`${VITE_API_BASE_URL}/menu`);
        //         const musicData = musicResponse.data.data;
        //         setMusic(musicData);
        //     } catch (error) {
        //         console.error('Error fetching data:', error);
        //     }
        // };
        // fetchData();
    }, []);

    return (
        <div className='container'>
            <h3 className='music-red-headings' >Music Collection</h3>
            {/* <div className='container'>
                <h1 className='red-headings'>Music Collection</h1>
                <div className="row">
                    {music.map((musics, index) => (
                        <div key={index} className="col-md-4 mb-4">
                            <div className="card">
                                <Link to={`/allcategory/${musics.menu_id}`}>
                                    <img src={musics.menu_image} className="card-img-top" alt="Banner" height={200} width={150} />
                                </Link>
                                <div className="card-body">
                                    <h4 className="card-title">{musics.menu_name}</h4>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div> */}

            <div className="main-card-container">
                <div className='music-card-container'>
              
                {music.map((musics, index) => (
                     <Link to={`/allcategory/${musics.menu_id}` }  style={{textDecoration:'none'}}>
                    <div className="music-card">
                        <div className="left">
                       
                                    <img src={musics.menu_image} className="card-img-top" alt="Banner" height={200} width={150} />
                                
                        </div>
                        <div className="right">
                        <h4 className='linkText'  >{musics.menu_name}</h4>
                        </div>
                    </div> 
                    </Link>
                 ))}    

                     {/* <div className="music-card">
                        <div className="left">

                        </div>
                        <div className="right">
                            
                        </div>
                    </div>               */}


                </div>

            </div>
        </div>
    );
}

export default Music;

// ---------------inside music car container
// {music.map((musics, index) => (   
//     <div class="grid-item">
//        <div class="content1">
//            {/* <div class="icon"><i class="fa fa-youtube-play"></i></div> */}
//            <img src={musics.menu_image} className="card-img-top" alt="Banner"  />
//            <div class="text">Guest Dj Mixes</div>
//        </div>
//     </div>
//     ))}