// import React, { useState, useEffect } from 'react';
// import axios from 'axios';
// import { useNavigate } from 'react-router-dom';
// import { VITE_API_BASE_URL } from '../api/env';

// const Entry = () => {
//   const navigate = useNavigate();
//   const [videoUrl, setVideoUrl] = useState('');
//   const [imageUrls, setImageUrls] = useState([]);
//   // const [userId, setUserId] = useState('');
//   // const [token, setToken] = useState('');
// //   let baseURL = process.env.baseURL || "https://topfloormusicapp.com/auth/api"

//   useEffect(() => {
//     const storedUserId = localStorage.getItem('userId');
//     const storedToken = localStorage.getItem('token');

//     if (storedUserId && storedToken) {
//       console.log('User credentials exist in local storage:', storedUserId, storedToken);
//       // If user credentials exist, redirect to home page
//     } else {
//       try {
//         // If user credentials do not exist, create a new user
//         const response = axios.post(`${VITE_API_BASE_URL}/appusers`,
//           {
//             device_type: 'web', // Replace with actual device type
//             token: '' // Assuming you have a state to hold device token
//           },
//           {
//             headers: {
//               'Content-Type': 'application/json',
//               'X-CSRFToken': 'Lq3QBB3zN62fw6IHDnw8ixaBX8ua030hPPdHN7mavqVMSu26Cb0PRWKQ1zSs4LVE',
//             }
//           }
//         );
//         // Set user credentials in local storage
//         localStorage.setItem('userId', response.data.user_id);
//         localStorage.setItem('token', response.data.token);
//         console.log('User created successfully:', response.data);
//         // Redirect to home page after successful registration
//       } catch (error) {
//         console.error('Error creating user:', error);
//       }}

//   }, []);

//   useEffect(() => {
//     // Fetch video URL
//     const fetchData = async () => {
//       try {
//         const response = await axios.get(`${VITE_API_BASE_URL}/splashvideo`);
//         const imageDataResponse = await axios.get(`${VITE_API_BASE_URL}/cms`);
//         const videoData = response.data.data;
//         const imageData = imageDataResponse.data.logo_data;
//         if (videoData.length > 0) {
//           setVideoUrl(videoData[0].splashvideo);
//           setImageUrls(imageData.map(data => data.image));

//         }
//       } catch (error) {
//         console.error('Error fetching video data:', error);
//       }
//     };
//     fetchData();
//   }, []);
//   const handleClick = async () => {
//     navigate('/home');
//     // Check if user credentials exist in local storage
//     // const storedUserId = localStorage.getItem('userId');
//     // const storedToken = localStorage.getItem('token');

//     // if (storedUserId && storedToken) {
//     //   console.log('User credentials exist in local storage:', storedUserId, storedToken);
//     //   // If user credentials exist, redirect to home page
//     //   navigate('/home');
//     // } else {
//     //   try {
//     //     // If user credentials do not exist, create a new user
//     //     const response = await axios.post(
//     //       `${VITE_API_BASE_URL}/appusers`,
//     //       {
//     //         device_type: 'web', // Replace with actual device type
//     //         token: '' // Assuming you have a state to hold device token
//     //       },
//     //       {
//     //         headers: {
//     //           'Content-Type': 'application/json',
//     //           'X-CSRFToken': 'Lq3QBB3zN62fw6IHDnw8ixaBX8ua030hPPdHN7mavqVMSu26Cb0PRWKQ1zSs4LVE',
//     //         }
//     //       }
//     //     );
//     //     // Set user credentials in local storage
//     //     localStorage.setItem('userId', response.data.user_id);
//     //     localStorage.setItem('token', response.data.token);
//     //     console.log('User created successfully:', response.data);
//     //     // Redirect to home page after successful registration
//     //     navigate('/home');
//     //   } catch (error) {
//     //     console.error('Error creating user:', error);
//     //   }
//     // }

//   };

//   return (
//     <>
//       <div className="background-video">
//         {videoUrl && (
//           <video autoPlay loop muted playsInline>
//             <source src={videoUrl} type="video/mp4" />
//             Your browser does not support the video tag.
//           </video>
//         )}
//         {imageUrls.map((imageUrl, index) => (
//         <div key={`image-${index}`}>
//           <img src={imageUrl} alt="Logo" />
//         </div>
//       ))}
//       </div>
//       {/* {imageUrls.map((imageUrl, index) => (
//         <div key={`image-${index}`}>
//           <img src={imageUrl} alt="Logo" />
//         </div>
//       ))} */}

//       <div>
//         <button onClick={handleClick}> Enter the site </button>
//       </div>
//     </>
//   );
// };

// export default Entry;

import React, { useState, useEffect } from "react";
import "../App.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { VITE_API_BASE_URL } from "../api/env";

const Entry = () => {
  const navigate = useNavigate();
  const [videoUrl, setVideoUrl] = useState("");
  const [imageUrls, setImageUrls] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch video URL
        const videoResponse = await axios.get(
          `${VITE_API_BASE_URL}/splashvideo`
        );
        const videoData = videoResponse.data.data;

        // Fetch image URLs
        const imageDataResponse = await axios.get(`${VITE_API_BASE_URL}/cms`);
        const imageData = imageDataResponse.data.logo_data;

        if (videoData.length > 0) {
          setVideoUrl(videoData[0].splashvideo);
          setImageUrls(imageData.map((data) => data.image));
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();

    // Check if user credentials exist in local storage
    const storedUserId = localStorage.getItem("userId");
    const storedToken = localStorage.getItem("token");

    if (!storedUserId || !storedToken) {
      createUser();
    } else {
      console.log(
        "User credentials exist in local storage:",
        storedUserId,
        storedToken
      );
      // Redirect to home page if user credentials exist
    }
  }, []);

  const createUser = async () => {
    try {
      // Create a new user
      const response = await axios.post(
        `${VITE_API_BASE_URL}/appusers`,
        {
          device_type: "web",
        },
        {
          headers: {
            "Content-Type": "application/json",
            "X-CSRFToken":
              "Lq3QBB3zN62fw6IHDnw8ixaBX8ua030hPPdHN7mavqVMSu26Cb0PRWKQ1zSs4LVE",
          },
        }
      );

      // Set user credentials in local storage
      localStorage.setItem("userId", response.data.user_id);
      localStorage.setItem("token", response.data.token);
      console.log("User created successfully:", response.data);

      // Redirect to home page after successful registration
      // navigate('/home');
    } catch (error) {
      console.error("Error creating user:", error);
    }
  };

  const handleClick = () => {
    navigate("/home");
  };

  return (
    <>
      <div className="background-video">
        {videoUrl && (
          <video autoPlay loop muted playsInline>
            <source src={videoUrl} type="video/mp4" />
            Your browser does not support the video tag.
          </video>
        )}
        {imageUrls.map((imageUrl, index) => (
          <div key={`image-${index}`} className="logo-container">
            <img src={imageUrl} alt="Logo" className="logo" />
          </div>
        ))}
      </div>

      <div className="button-container">
        <button className="enter-button" onClick={handleClick}>
          Enter the site
        </button>
      </div>
    </>
  );
};

export default Entry;
