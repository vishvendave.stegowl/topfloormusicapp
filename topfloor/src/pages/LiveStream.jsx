import React, { useState, useEffect } from "react";
import axios from "axios";
import Chat from "../components/Chat";
import { VITE_API_BASE_URL } from "../api/env";
import ReactPlayer from "react-player";
import { useNavigate } from "react-router-dom";
import logo from "../assets/images/logo.png";
import liveStreamAPI from '../services/homeServices'

function LiveStream() {
  const [livestreamData, setLivestreamData] = useState(null);
  const [additionalData, setAdditionalData] = useState(null);
  const [error, setError] = useState(null);
  const [selectedSource, setSelectedSource] = useState("otherStream");
  const [videoUrl, setVideoUrl] = useState("");
  const navigate = useNavigate();
  const userId = localStorage.getItem("userId");

  useEffect(() => {

    window.scrollTo(0, 0);

  }, []);
  useEffect(() => {

    liveStreamAPI.fetchLiveStreamData().then((res)=>{
      setLivestreamData(res.data);
    }).catch((error)=>{
      setError("Error fetching livestream data");
      console.error("Error fetching livestream data:", error);
    })

    liveStreamAPI.fetchAdditionalData().then((res)=>{
      setAdditionalData(res.data);
    })

    // const userId = localStorage.getItem("userId");
    // const token = localStorage.getItem("token");

    // if (!userId || !token) {
    //   navigate("/");
    // }
    // const fetchLivestreamData = async () => {
    //   try {
    //     const response = await axios.get(`${VITE_API_BASE_URL}/livestream`);
    //     setLivestreamData(response.data.data);
    //   } catch (error) {
    //     setError("Error fetching livestream data");
    //     console.error("Error fetching livestream data:", error);
    //   }
    // };

    // const fetchAdditionalData = async () => {
    //   try {
    //     const response = await axios.post(
    //       `${VITE_API_BASE_URL}/livevideo`,
    //       {
    //         user_id: userId,
    //       },
    //       {
    //         headers: {
    //           "Content-Type": "application/json",
    //         },
    //       }
    //     );
    //     setAdditionalData(response.data.data);
    //   } catch (error) {
    //     setError("Error fetching additional data");
    //     console.error("Error fetching additional data:", error);
    //   }
    // };

    // fetchLivestreamData();
    // fetchAdditionalData();
  }, [userId]);

  useEffect(() => {
    if (selectedSource === "topFloor" && livestreamData) {
      setVideoUrl(livestreamData[0].livestream_url);
    } else if (selectedSource === "otherStream" && additionalData) {
      setVideoUrl(additionalData[0].url);
    }
  }, [selectedSource, livestreamData, additionalData]);

  const handleSourceChange = (source) => {
    setSelectedSource(source);
  };

  const handleImageError = () => {
    // Set the fallback image source
    setImageSrc(logo);
  };

  return (
    <div >
      {/* <div className="live-outer">
        <div className="live-stream-container">
          <div className="live-stream-video-container">
            <ReactPlayer
              url={videoUrl}
              playing
              controls
             
            />
          </div>
          <Chat />
        </div>
      </div> */}
       {/* <div className="contain-stream">
          <div className="contain-video">
          <ReactPlayer
              url={videoUrl}
              playing
              controls
             
            />
          </div>
          <div className="contain-chat">
          <Chat />
          </div>
      </div> */}

      <div className="contain-stream">
          <div className="main-live-card-container">
            <div className="theater-container main-live-card-container " id='theater'>
              <div className="main-player">
              
                  <ReactPlayer
                    url={videoUrl}
                    playing
                    controls
                    width="100%" 
                    height="100%" 
                   
             
                  />
                

              </div>

              <div className="right-chat">
                <Chat/>
              </div>

            </div>
         
          </div>
          
      </div>


      <div className="live-card-section">
        <div className="main-live-card-container">
          <div className="live-title">
            <h2>Top floor Live Stream</h2>
            <h6>Top floor Live Stream</h6>
          </div>

          <div className="live-card-container">
            <div className="card-container">
              <div className="live-card">
                <button onClick={() => handleSourceChange('topFloor')} className="live-button">
                  {livestreamData && livestreamData.length > 0 && (
                    <div className="live-inner-card">
                      <img src={livestreamData[0].livestream_image} onError={(e) => { e.target.onerror = null; e.target.src = logo; }} className="card-img-top" alt="" />
                      <p className="live-stream-cards-text">Dj Angels S 10</p>
                    </div >
                  )}
                </button>
              </div>


              <div className="live-card-x">
                <div onClick={() => handleSourceChange('otherStream')} className="live-card-x-inner">
                  {additionalData && additionalData.length > 0 && (
                    <div className="imgs live-card-x-inner">
                      <img src={additionalData[0].image} onError={(e) => { e.target.onerror = null; e.target.src = logo; }} height="150" width="150" alt="" />
                      <p className="live-stream-cards-text">Dj Angels S 10</p>
                      
                    </div>
                    
                  )}
                </div>
              </div>
              {/* <div className="live-card">
                <button onClick={() => handleSourceChange('otherStream')}>
                  {additionalData && additionalData.length > 0 && (
                      <div className="live-inner-card2">
                      <img src={additionalData[0].image} onError={(e) => { e.target.onerror = null; e.target.src = logo; }} height="150" width="150" alt="" />
                      <p>Dj Angels S 10</p>
                    </div>
                  )}
                </button>
              </div> */}

              {/* <div>
                                 <button onClick={() => handleSourceChange('topFloor')}>
                                 {livestreamData && livestreamData.length > 0 && (
                                     <img src={livestreamData[0].livestream_image} onError={(e) => { e.target.onerror = null; e.target.src = logo; }} height="150" width="150" alt="" />
                                 )}
                                 </button> Top Floor
                                 <br /> <br />
                                 <button onClick={() => handleSourceChange('otherStream')}>
                                 {additionalData && additionalData.length > 0 && (
                                     <img src={additionalData[0].image} onError={(e) => { e.target.onerror = null; e.target.src = logo; }} height="150" width="150" alt="" />
                                 )}
                                 </button> Top Floor Live Stream
                     </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>

    // <div className="container">
    //     <div className='stream-container'>
    //         <div className="streaming">
    //             <div className="video-stream">

    //             <ReactPlayer
    //                         url={videoUrl}
    //                         playing
    //                         controls
    //                         width="100%"
    //                         height="100%"
    //                     />
    //             </div>
    //             <div className="chat-stream">

    //             </div>
    //         </div>

    //     </div>
    // </div>

    // <div className="container-fluid">
    //     <div className="row">
    //         <div className="col-sm-6">
    //             <div>
    //                 <ReactPlayer
    //                     url={videoUrl}
    //                     playing
    //                     controls
    //                     width="100%"
    //                     height="auto"
    //                 />
    //                 {selectedSource === 'topFloor' && (
    //                     <div>
    //                         <h1>{livestreamData && livestreamData[0].livestream_name}</h1>
    //                         <p>{livestreamData && livestreamData[0].livestream_description}</p>
    //                         {/* <h1>{livestreamData && livestreamData[0].}</h1> */}
    //                     </div>
    //                 )}
    //                 {selectedSource === 'otherStream' && (
    //                     <div>
    //                         <h1>{additionalData && additionalData[0].name}</h1>
    //                         <p>{additionalData && additionalData[0].description}</p>
    //                     </div>
    //                 )}
    //             </div>
    //         </div>
    //         <div className="col-sm-6">
    //             {/* <div>
    //                 <button onClick={() => handleSourceChange('topFloor')}>
    //                     <img src="https://topfloor.s3.amazonaws.com/uploads/assets/main_logo.png" height="150" width="150"  alt="" />
    //                 </button> Top Floor
    //                 <br /> <br />
    //                 <button onClick={() => handleSourceChange('otherStream')}>
    //                     <img src="https://topfloor.s3.amazonaws.com/uploads/assets/main_logo.png" height="150" width="150"  alt="" />
    //                 </button>  Top Floor Live Stream
    //             </div> */}
    //              <div>
    //                         <button onClick={() => handleSourceChange('topFloor')}>
    //                         {livestreamData && livestreamData.length > 0 && (
    //                             <img src={livestreamData[0].livestream_image} onError={(e) => { e.target.onerror = null; e.target.src = logo; }} height="150" width="150" alt="" />
    //                         )}
    //                         </button> Top Floor
    //                         <br /> <br />
    //                         <button onClick={() => handleSourceChange('otherStream')}>
    //                         {additionalData && additionalData.length > 0 && (
    //                             <img src={additionalData[0].image} onError={(e) => { e.target.onerror = null; e.target.src = logo; }} height="150" width="150" alt="" />
    //                         )}
    //                         </button> Top Floor Live Stream
    //             </div>
    //         </div>
    //     </div>
    //     <center> <Chat /> </center>
    //     <div>
    //     </div>
    // </div>
  );
}

export default LiveStream;
