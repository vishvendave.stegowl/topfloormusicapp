import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { VITE_API_BASE_URL } from '../api/env';
import { useNavigate } from 'react-router-dom';
import eventAPI from '../services/homeServices'

export default function Events() {
    const [events, setEvents] = useState([]);
    const [selectedEvent, setSelectedEvent] = useState(null);
    const navigate = useNavigate();
    useEffect(() => {

        window.scrollTo(0, 0);

    }, []);
    useEffect(() => {
        eventAPI.fetchEventData().then((res)=>{
            const eventData = res.data;
            setEvents(eventData);
        })
        // const fetchData = async () => {
        //     try {
        //         const userId = localStorage.getItem('userId');
        //         const token = localStorage.getItem('token');

        //         if (!userId || !token) {
        //             navigate('/');
        //         }

        //         const eventResponse = await axios.get(`${VITE_API_BASE_URL}/liveevent`);
        //         const eventData = eventResponse.data.data;
        //         setEvents(eventData);
        //     } catch (error) {
        //         console.error('Error fetching data:', error);
        //     }
        // };
        // fetchData();
    }, []);

    const handleEventClick = (selectedEvent) => {
        setSelectedEvent(selectedEvent);
    };

    return (
        <div className="container" style={{ paddingLeft:'8%'}}>
            <div >
                <h3 className='event-heading'>Live Event</h3>
                <div className="row justify-content-start">
                    {events.map((event, index) => (
                        <div key={`banner-${index}`} className="col-md-2 mb-2 event-container">
                            <div className="card " id='event-card' style={{ cursor: 'pointer' }} onClick={() => handleEventClick(event)}>
                                <div className='card-body event-card-img'>
                                    <img src={event.event_image} className="card-img-top" alt="Banner" height={150} width={150} />
                                </div>
                                <div className='event-desc'>
                                    <h6 className='event-venue'>{event.event_venue}</h6>
                                    <h6 className='event-name'>{event.event_name}</h6>
                                    <p className='event-date' style={{color:'white'}}>{event.event_date}</p>
                                    <p className='event-para' style={{color:'white'}}>{event.event_start_time} - {event.event_end_time}</p>
                                    <p className='event-para' style={{color:'white'}}>{event.event_location}</p>

                                </div>
                                {/* <div className="card-body event-card-body justify-content-start">
                                    <h2 className="card-title d-flex justify-content-start">{event.event_venue}</h2>
                                    <h3 className="white card-title d-flex justify-content-start">{event.event_name}</h3>
                                    <p className="white p-0 d-flex justify-content-start">{event.event_date}</p>
                                    <p className="white p-0 d-flex justify-content-start">{event.event_start_time} - {event.event_end_time}</p>
                                    <p className="white p-0 d-flex justify-content-start">{event.event_location}</p>
                                </div> */}
                            </div>
                        </div>
                    ))}
                </div>
                {selectedEvent && (
                <div className='radio-main-modal'>

                    <div className='radio-modal-content'>
                        <span className='close-button' onClick={() => setSelectedEvent(null)}>X</span>
                        <div className="modal-image">
                            <img src={selectedEvent.event_image} className="card-img-top" alt="Banner" />
                        </div>
                        <div className="modal-details">

                            <h3 className='radio-modal-text-r' >{selectedEvent.event_name}</h3>
                            <p className='radio-modal-text '>{selectedEvent.event_venue}</p>
                            <p>{selectedEvent.event_date}</p>
                            <p className='radio-modal-text small'>{selectedEvent.event_start_time} - {selectedEvent.event_end_time}</p>
                            <p className="red">{selectedEvent.event_location}</p>
                            <p className='radio-modal-text-r'>{selectedEvent.event_description}</p>
                            {/* <button className="btn btn-danger" onClick={handleCloseDetail}>Close</button> */}

                        </div>
                    </div>

                </div>
                    // <div className="position-fixed top-50 start-50 translate-middle event-popup-card" style={{}}>
                    //     <div className='p-3'>
                    //         <img src={selectedEvent.event_image} className="card-img-top" alt="Banner" height={400} width={400} />
                    //     </div>
                    //     <div className='p-3'>
                    //         <h2 className='white'>{selectedEvent.event_name}</h2>
                    //         <p className="red">{selectedEvent.event_venue}</p>
                    //         <p>{selectedEvent.event_date}</p>
                    //         <p>{selectedEvent.event_start_time} - {selectedEvent.event_end_time}</p>
                    //         <p className="red">{selectedEvent.event_location}</p>
                    //         <p className="red">{selectedEvent.event_description}</p>
                    //         <button className="btn btn-danger event-cross-btn" onClick={() => setSelectedEvent(null)}>X</button>
                    //     </div>
                    // </div>

                )}
            </div>
        </div>
    );
}
