import React, { useState, useEffect } from 'react';
import Information from '../components/Information';
import { useNavigate } from 'react-router-dom';

function Info() {

  const navigate = useNavigate();
  const [bannerUrl, setBannerUrl] = useState('');

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  useEffect(() => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        navigate('/');
      }
    } catch (error) {
      console.error('Error accessing local storage:', error);
    }
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('https://topfloormusicapp.com/auth/api/booking');
        const data = await response.json();
        if (response.ok) {
          setBannerUrl(data.data);
        } else {
          // Handle error
          console.error('Failed to fetch data:', data.message);
        }
      } catch (error) {
        // Handle network error
        console.error('Failed to fetch data:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <div>

      {/* <div>
        {bannerUrl ? (
          <iframe src={bannerUrl} title="Sponsor Banner" ></iframe>
        ) : (
          <p>Loading...</p>
        )}
      </div> */}
      {/* <div>
      {bannerUrl ? (
        <iframe src={bannerUrl} title="Sponsor Banner" ></iframe>
      ) : (
        <p>Loading...</p>
      )}
    </div> */}



      {/* <Information /> */}


      <Information bannerUrl={bannerUrl} />

    </div>
  );
}

export default Info;
