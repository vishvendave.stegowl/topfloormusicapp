import React, { useEffect, useState } from "react";

import axios from "axios";
import getHome from "../services/homeServices";

import { VITE_API_BASE_URL } from "../api/env";
import Banner from "../components/Banner";
import Trendings from "../components/Trendings";
import LatestMixes from "../components/LatestMixes";
import Navbar from "../components/Navbar";
import RadioAP from "../components/RadioAP";
import Footer from "../components/Footer";
import HomeSlider from "../components/HomeSlider";
import { useNavigate } from "react-router-dom";

export default function Home() {
  const [imageUrls, setImageUrls] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
    // const element = document.getElementById("header");
    // const element = document.getElementById("header");
    // element.scrollIntoView(true);
    window.scrollTo(0, 0);
  }, []);
  useEffect(() => {
    // const fetchData = async () => {
    //   try {
    //     const userId = localStorage.getItem('userId');
    //     const token = localStorage.getItem('token');
    //     if (!userId || !token) {
    //       navigate('/');
    //     }
    //     const imageDataResponse = await axios.get(`${VITE_API_BASE_URL}/cms`);
    //     const imageData = imageDataResponse.data.logo_data;
    //     setImageUrls(imageData.map(data => data.image));
    //   }
    //   catch (error) {
    //     console.error('Error fetching image data:', error);
    //   }
    // };
    // fetchData();
    // getHome.getHomeData().then((res)=>{
    //   cont
    // })
  }, []);

  return (
    <>
      <div className="container">
        {/* <div>
      <center>
      {imageUrls.map((imageUrl, index) => (
        <div key={`image-${index}`}>
          <img src={imageUrl} alt="Logo" />
        </div>
      ))}
      </center>
      </div> */}
        <HomeSlider />
        <h5 className="red-headings"> Trendings </h5>
        <Trendings />

        <h5 className="red-headings large"> Latest Mixes </h5>
        <LatestMixes />

        <h5 className="red-headings large">Radio And Podcast</h5>
        <RadioAP />
      </div>
    </>
  );
}
