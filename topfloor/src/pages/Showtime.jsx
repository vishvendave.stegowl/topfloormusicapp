import React, { useEffect } from "react";
import RadioAP from "../components/RadioAP";
import Navbar from "../components/Navbar";
import Banner from "../components/Banner";
import Footer from "../components/Footer";
import { useNavigate } from "react-router-dom";

export default function Showtime() {
  const navigate = useNavigate();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  useEffect(() => {
    try {
      const userId = localStorage.getItem("userId");
      const token = localStorage.getItem("token");

      if (!userId || !token) {
        navigate("/");
      }
    } catch (error) {
      console.error("Error accessing local storage:", error);
    }
  }, []);
  return (
    <div className="contain-stream">
      <div className="showtime-container">
        <h4 className="showtime-heading">Daily Shows</h4>
        <RadioAP />
      </div>
    </div>
  );
}
