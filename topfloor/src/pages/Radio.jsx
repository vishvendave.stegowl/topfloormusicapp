import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Navbar from '../components/Navbar';
import Banner from '../components/Banner';
import { VITE_API_BASE_URL } from '../api/env';
import Footer from '../components/Footer';
import { setSelectedSong, setSongs } from '../redux/MusicSlice';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import radioAPI from '../services/homeServices'
import playImg from '../assets/images/play-icon2.png'

export default function Radio() {
  const dispatch = useDispatch();
  const [music, setMusic] = useState([]);

  const [radioData, setRadioData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const navigate = useNavigate();
  useEffect(() => {

    window.scrollTo(0, 0);

  }, []);
  
  useEffect(() => {
    radioAPI.fetchRadioData().then((res)=>{
      setRadioData(res.data);
      setLoading(false);
    })
    .catch((error)=>{
      setError(error.message);
        setLoading(false);
    })
    // const fetchData = async () => {
    //   try {
    //     const userId = localStorage.getItem('userId');
    //     const token = localStorage.getItem('token');

    //     if (!userId || !token) {
    //       navigate('/');
    //     }

    //     const response = await axios.get(`${VITE_API_BASE_URL}/liveradio`, {
    //       headers: {
    //         'Accept': 'application/json',
    //         'X-CSRFToken': 'xyz'
    //       }
    //     });
    //     setRadioData(response.data.data);
    //     setLoading(false);
    //   } catch (error) {
    //     setError(error.message);
    //     setLoading(false);
    //   }
    // };

    // fetchData();
  }, []);

  if (error) {
    return <div>Error: {error}</div>;
  }

  const handleImageClick = (radio)=> {
    dispatch(setSelectedSong(radio.song_id));
    dispatch(setSongs([radio])); // Assuming radio represents a song object
  };

  return (
    <div className='live-radio-wrapper'>
    {loading ? (
      <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    ) : (
      radioData.length > 0 ? (
        
      <div className='radion-div'>
         {radioData.map((radio, index) => (
        <div className="r-container">
          <div className="img-radio">
            <div className="push-play">
              
              <img src={playImg} onClick={() => handleImageClick(radio)} alt="assets" />
            </div>
            <div className="radio-title">Top floor Radio</div>
          </div>
        </div>
         ))}
      </div>
      ) : (
        <div>No radio stations found.</div>
      )
    )}
  </div>
    // <div>
    //   {loading ? (
    //     <div className="d-flex justify-content-center">
    //       <div className="spinner-border" role="status">
    //         <span className="visually-hidden">Loading...</span>
    //       </div>
    //     </div>
    //   ) : (
    //     radioData.length > 0 ? (
    //       <center className='d-flex justify-content-center'>
    //         {radioData.map((radio, index) => (
    //           <div className='radio-container p-2' key={index}>
    //             <div>
    //                {/* <img style={{ height: '250px' }} src={radio.song_image} alt={radio.song_name} /> */}
    //                <img 
    //               src={radio.song_image} 
    //               alt={radio.song_name}
    //               onClick={() => handleImageClick(radio)}
    //             />
    //             </div>

    //             {/* <audio controls>
    //               <source src={radio.song} type="audio/mpeg" />
    //               Your browser does not support the audio element.
    //             </audio> */}
    //             <h3 className='red'>{radio.song_name}</h3>
    //           </div>
    //         ))}
    //       </center>
    //     ) : (
    //       <div>No radio stations found.</div>
    //     )
    //   )}
    // </div>
  );
}
