import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setSelectedSong, selectNextSong, selectPreviousSong } from '../redux/MusicSlice';
import H5AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';
import playlistImg from '../assets/images/playlist.jpg'
import shuffle from '../assets/images/shuffle2.png'
import repeate from '../assets/images/repeate2.png'
import repeateOff from '../assets/images/repeateOff.png'
import shuffleOff from '../assets/images/shuffleOff.png'

const MusicPlayer = () => {
  // const dispatch = useDispatch();
  // const selectedSongIndex = useSelector(state => state.music.selectedSongIndex);
  // const songs = useSelector(state => state.music.songs);

  // const [showPlaylist, setShowPlaylist] = useState(false);
  // const [isLivestream, setIsLivestream] = useState(false);

  // const currentSongIndex = songs.findIndex(song => song.song_id === selectedSongIndex);
  // const currentSong = songs[currentSongIndex];
  // const audioRef = useRef(null);

  // useEffect(() => {
  //   const audioPlayer = audioRef.current.audio.current;
  //   const handleAudioEnded = () => {
  //     console.log('Song ended, selecting next song...');
  //     const nextIndex = (currentSongIndex + 1) % songs.length;
  //     dispatch(setSelectedSong(songs[nextIndex].song_id));
  //   };

  //   audioPlayer.addEventListener('ended', handleAudioEnded);

  //   return () => {
  //     audioPlayer.removeEventListener('ended', handleAudioEnded);
  //   };
  // }, [dispatch, currentSongIndex, songs]);

  // useEffect(() => {
  //   const audioPlayer = audioRef.current.audio.current;

  //   const handleMetadataLoaded = () => {
  //     console.log('Audio metadata loaded, playing...');
  //     audioPlayer.play();
  //   };

  //   audioPlayer.addEventListener('loadedmetadata', handleMetadataLoaded);

  //   return () => {
  //     audioPlayer.removeEventListener('loadedmetadata', handleMetadataLoaded);
  //   };
  // }, [currentSongIndex, songs]);

  // useEffect(() => {
  //   setIsLivestream(location.pathname === '/livestream');
  // }, [location.pathname]);

  // useEffect(() => {
  //   const audioPlayer = audioRef.current.audio.current;

  //   if (isLivestream && !audioPlayer.paused) {
  //     audioPlayer.pause();
  //   }
  // }, [isLivestream]);

  // const handleNext = () => {
  //   console.log('Next button clicked');
  //   const nextIndex = (currentSongIndex + 1) % songs.length;
  //   dispatch(setSelectedSong(songs[nextIndex].song_id));
  // };

  // const handlePrevious = () => {
  //   console.log('Previous button clicked');
  //   const previousIndex = (currentSongIndex - 1 + songs.length) % songs.length;
  //   dispatch(setSelectedSong(songs[previousIndex].song_id));
  // };

  // const togglePlaylist = () => {
  //   setShowPlaylist(!showPlaylist);
  // };

  // const dispatch = useDispatch();
  // const selectedSongIndex = useSelector(state => state.music.selectedSongIndex);
  // const songs = useSelector(state => state.music.songs);

  // const [showPlaylist, setShowPlaylist] = useState(false);
  // const [isLivestream, setIsLivestream] = useState(false);
  // const [shuffleMode, setShuffleMode] = useState(false); // State for shuffle mode

  // const currentSongIndex = songs.findIndex(song => song.song_id === selectedSongIndex);
  // const currentSong = songs[currentSongIndex];
  // const audioRef = useRef(null);

  // useEffect(() => {
  //   const audioPlayer = audioRef.current.audio.current;
  //   const handleAudioEnded = () => {
  //     console.log('Song ended, selecting next song...');
  //     if (shuffleMode) {
  //       const nextIndex = Math.floor(Math.random() * songs.length);
  //       dispatch(setSelectedSong(songs[nextIndex].song_id));
  //     } else {
  //       const nextIndex = (currentSongIndex + 1) % songs.length;
  //       dispatch(setSelectedSong(songs[nextIndex].song_id));
  //     }
  //   };

  //   audioPlayer.addEventListener('ended', handleAudioEnded);

  //   return () => {
  //     audioPlayer.removeEventListener('ended', handleAudioEnded);
  //   };
  // }, [dispatch, currentSongIndex, songs, shuffleMode]);

  // const handleNext = () => {
  //   console.log('Next button clicked');
  //   if (shuffleMode) {
  //     const nextIndex = Math.floor(Math.random() * songs.length);
  //     dispatch(setSelectedSong(songs[nextIndex].song_id));
  //   } else {
  //     const nextIndex = (currentSongIndex + 1) % songs.length;
  //     dispatch(setSelectedSong(songs[nextIndex].song_id));
  //   }
  // };

  // const handlePrevious = () => {
  //   console.log('Previous button clicked');
  //   const previousIndex = (currentSongIndex - 1 + songs.length) % songs.length;
  //   dispatch(setSelectedSong(songs[previousIndex].song_id));
  // };

  // const togglePlaylist = () => {
  //   setShowPlaylist(!showPlaylist);
  // };

  // const toggleShuffle = () => {
  //   setShuffleMode(!shuffleMode);
  // };

  const dispatch = useDispatch();
  const selectedSongIndex = useSelector(state => state.music.selectedSongIndex);
  const songs = useSelector(state => state.music.songs);

  const [showPlaylist, setShowPlaylist] = useState(false);
  const [isLivestream, setIsLivestream] = useState(false);
  const [shuffleMode, setShuffleMode] = useState(false); // State for shuffle mode
  const [repeatMode, setRepeatMode] = useState(false); // State for repeat mode

  const currentSongIndex = songs.findIndex(song => song.song_id === selectedSongIndex);
  const currentSong = songs[currentSongIndex];
  const audioRef = useRef(null);

  useEffect(() => {
    const audioPlayer = audioRef.current.audio.current;
    const handleAudioEnded = () => {
      console.log('Song ended, selecting next song...');
      if (repeatMode) {
        audioRef.current.audio.current.play();
      } else if (shuffleMode) {
        const nextIndex = Math.floor(Math.random() * songs.length);
        dispatch(setSelectedSong(songs[nextIndex].song_id));
      } else {
        const nextIndex = (currentSongIndex + 1) % songs.length;
        dispatch(setSelectedSong(songs[nextIndex].song_id));
      }
    };

    audioPlayer.addEventListener('ended', handleAudioEnded);

    return () => {
      audioPlayer.removeEventListener('ended', handleAudioEnded);
    };
  }, [dispatch, currentSongIndex, songs, shuffleMode, repeatMode]);

  const handleNext = () => {
    console.log('Next button clicked');
    if (shuffleMode) {
      const nextIndex = Math.floor(Math.random() * songs.length);
      dispatch(setSelectedSong(songs[nextIndex].song_id));
    } else {
      const nextIndex = (currentSongIndex + 1) % songs.length;
      dispatch(setSelectedSong(songs[nextIndex].song_id));
    }
  };

  const handlePrevious = () => {
    console.log('Previous button clicked');
    const previousIndex = (currentSongIndex - 1 + songs.length) % songs.length;
    dispatch(setSelectedSong(songs[previousIndex].song_id));
  };

  const togglePlaylist = () => {
    setShowPlaylist(!showPlaylist);
  };

  const toggleShuffle = () => {
    setShuffleMode(!shuffleMode);
  };

  const toggleRepeat = () => {
    setRepeatMode(!repeatMode);
  };


  return (
    <div style={{ position: 'relative' }}>
      {showPlaylist && (
        <div className="main-show-palylist">

          <div className="playlist-card">
            <button className='close-btn' onClick={togglePlaylist}>x</button>
            <div className="display-playlist">
              {songs.map(song => (
                <div key={song.song_id} className='songs-card'>
                  <div className='songs-card-info'>
                    <img src={song.song_image} alt={song.song_name} style={{ height: '80px', width: '80px', borderRadius: '50%', marginRight: '10px', display: 'inline' }} />
                    <p style={{ display: 'inline' }}>{song.song_name}</p>
                  </div>

                  <button className='songs-card-btn' onClick={() => dispatch(setSelectedSong(song.song_id))}>Play</button>
                </div>
              ))}
            </div>

          </div>
        </div>
      )}


      {/* {showPlaylist && (
        <div style={{ position: 'fixed', top: '90px', left: '50px', right: '50px', maxHeight: 'calc(100vh - 100px)', overflowY: 'auto', background: 'rgba(255, 255, 255, 0.8)', padding: '10px', borderRadius: '5px', boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)' }}>
          <button style={{ position: 'absolute', top: '5px', right: '50%', transform: 'translateX(50%)' }} onClick={togglePlaylist}>Hide</button><br /><br />
          {songs.map(song => (
            <div key={song.song_id} style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
              <img src={song.song_image} alt={song.song_name} style={{ height: '40px', width: '40px', borderRadius: '50%', marginRight: '10px' }} />
              <p>{song.song_name}</p>
              <button onClick={() => dispatch(setSelectedSong(song.song_id))}>Play</button>
            </div>
          ))}
        </div>
      )} */}

      {/* <div style={{ position: 'fixed', bottom: '0px', width: '100%', zIndex: '999', background: 'white' }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {currentSong && (
            <div style={{ display: 'flex', alignItems: 'center', marginRight: '20px' }}>
              <img src={currentSong.song_image} alt={currentSong.song_name} style={{ height: '40px', width: '40px', borderRadius: '50%', marginRight: '10px' }} />
              <p>{currentSong.song_name}</p>
            </div>
          )}
          <H5AudioPlayer
            autoPlay
            style={{ background: 'white' }}
            src={currentSong ? currentSong.song : ''}
            showJumpControls={false}
            customAdditionalControls={[
              <button key="previous" onClick={handlePrevious} disabled={isLivestream}>Previous</button>,
              <button key="next" onClick={handleNext} disabled={isLivestream}>Next</button>,
            ]}
            ref={audioRef}
            onPlay={() => {
              if (isLivestream) {
                audioRef.current.audio.current.pause();
              }
            }}
          />
          <button onClick={togglePlaylist}>{showPlaylist ? 'Hide Playlist' : 'Show Playlist'}</button>
        </div>
      </div> */}

      {/* <div className='player-container' >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {currentSong && (
            <div style={{ display: 'flex', alignItems: 'center', marginRight: '20px' }}>
              <img src={currentSong.song_image} alt={currentSong.song_name} style={{ height: '40px', width: '40px', borderRadius: '50%', marginRight: '10px' }} />
              <p>{currentSong.song_name}</p>
            </div>
          )}
          <H5AudioPlayer
            autoPlay
            style={{ background: 'white' }}
            src={currentSong ? currentSong.song : ''}
            showJumpControls={false}
            customAdditionalControls={[
              <button key="previous" onClick={handlePrevious} disabled={isLivestream}>Previous</button>,
              <button key="next" onClick={handleNext} disabled={isLivestream}>Next</button>,
            ]}
            ref={audioRef}
            onPlay={() => {
              if (isLivestream) {
                audioRef.current.audio.current.pause();
              }
            }}
          />
          <button onClick={togglePlaylist}>{showPlaylist ? 'Hide Playlist' : 'Show Playlist'}</button>
        </div>
      </div> */}

      <div className='player-container'>
        <div className='music-player'>
          {currentSong && (
            <div className='song-info'>
              <img src={currentSong.song_image} alt={currentSong.song_name} className='album-art' />
              <p className='song-name'>{currentSong.song_name}</p>
            </div>
          )}
          <H5AudioPlayer
            autoPlay
            className='audio-player'
            src={currentSong ? currentSong.song : ''}

            showSkipControls={true}
            onClickPrevious={handlePrevious}
            onClickNext={handleNext}
            showJumpControls={false}

            customAdditionalControls={[
              <button className='iconBtn' key="shuffleButton" onClick={toggleShuffle}>
                {shuffleMode ? <img src={shuffle} className='suffleIcon' alt="" /> : <img src={shuffleOff} alt="" className='suffleIcon' />}
              </button>,
              <button className='iconBtn' key="repeatButton" onClick={toggleRepeat}>
                {repeatMode ? <img src={repeate} className='repeateIcon' alt="" /> : <img src={repeateOff} className='repeateIcon' alt="" />}
              </button>
            ]}

            ref={audioRef}
            onPlay={() => {

              if (isLivestream) {
                audioRef.current.audio.current.pause();
              }
            }}

          />


          <button   className='playlist-toggle' onClick={togglePlaylist}>
            {showPlaylist ? <img src={playlistImg} alt='Show Playlist' style={{height:'30px', width:'30px'}}/> :
            <img src={playlistImg} alt='Show Playlist' style={{height:'30px', width:'30px'}}/> }
          </button>
        </div>
      </div>

    </div>
  );
};

export default MusicPlayer;




// import React, { useState, useEffect, useRef } from 'react';
// import { useSelector, useDispatch } from 'react-redux';
// import { setSelectedSong, selectNextSong, selectPreviousSong } from '../redux/MusicSlice';
// import H5AudioPlayer from 'react-h5-audio-player';
// import 'react-h5-audio-player/lib/styles.css';

// const MusicPlayer = () => {
//   const dispatch = useDispatch();
//   const selectedSongIndex = useSelector(state => state.music.selectedSongIndex);
//   const songs = useSelector(state => state.music.songs);

//   const [showPlaylist, setShowPlaylist] = useState(false);

//   const currentSongIndex = songs.findIndex(song => song.song_id === selectedSongIndex);
//   const currentSong = songs[currentSongIndex];
//   const audioRef = useRef(null);

//   useEffect(() => {
//     const audioPlayer = audioRef.current.audio.current;
//     const handleAudioEnded = () => {
//       console.log('Song ended, selecting next song...');
//       // const nextIndex = (currentSongIndex + 1) % songs.length;
//       // dispatch(setSelectedSong(songs[nextIndex].song_id));
//       const nextIndex = (currentSongIndex + 1) % songs.length;
//       if (nextIndex !== 0 && nextIndex !== currentSongIndex) {
//         dispatch(setSelectedSong(songs[nextIndex].song_id));
//       }

//     };

//     audioPlayer.addEventListener('ended', handleAudioEnded);

//     return () => {
//       audioPlayer.removeEventListener('ended', handleAudioEnded);
//     };
//   }, [dispatch, currentSongIndex, songs]);

//   useEffect(() => {
//     const audioPlayer = audioRef.current.audio.current;

//     const handleMetadataLoaded = () => {
//       console.log('Audio metadata loaded, playing...');
//       audioPlayer.play();
//     };

//     audioPlayer.addEventListener('loadedmetadata', handleMetadataLoaded);

//     return () => {
//       audioPlayer.removeEventListener('loadedmetadata', handleMetadataLoaded);
//     };
//   }, [currentSongIndex, songs]);


//   useEffect(() => {
//     const audioPlayer = audioRef.current.audio.current;

//     if (location.pathname === '/livestream') {
//       audioPlayer.pause();
//     }
//   }, [location.pathname]);


//   // const handleNext = () => {
//   //   console.log('Next button clicked');
//   //   const nextIndex = (currentSongIndex + 1) % songs.length;
//   //   dispatch(setSelectedSong(songs[nextIndex].song_id));
//   // };

//   // const handlePrevious = () => {
//   //   console.log('Previous button clicked');
//   //   const previousIndex = (currentSongIndex - 1 + songs.length) % songs.length;
//   //   dispatch(setSelectedSong(songs[previousIndex].song_id));
//   // };

//   const handleNext = () => {
//     console.log('Next button clicked');
//     const nextIndex = (currentSongIndex + 1) % songs.length;
//     if (nextIndex !== 0 && nextIndex !== currentSongIndex) {
//       dispatch(setSelectedSong(songs[nextIndex].song_id));
//     }
//   };

//   const handlePrevious = () => {
//     console.log('Previous button clicked');
//     const previousIndex = (currentSongIndex - 1 + songs.length) % songs.length;
//     if (previousIndex !== currentSongIndex && previousIndex !== (songs.length - 1)) {
//       dispatch(setSelectedSong(songs[previousIndex].song_id));
//     }
//   };


//   const togglePlaylist = () => {
//     setShowPlaylist(!showPlaylist);
//   };

//   return (
//     <div style={{ position: 'relative' }}>
//       {showPlaylist && (
//         <div style={{ position: 'fixed', top: '90px', left: '50px', right: '50px', maxHeight: 'calc(50vh - 0px)', overflowY: 'auto', background: 'rgba(255, 255, 255, 0.8)', padding: '10px', borderRadius: '5px', boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',alignItems:'center' }}>
//           <button style={{ position: 'absolute', top: '5px', right: '50%', transform: 'translateX(50%)' }} onClick={togglePlaylist}>Hide</button><br /><br />
//           {songs.map(song => (
//             <div key={song.song_id} style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
//               <img src={song.song_image} alt={song.song_name} style={{ height: '40px', width: '40px', borderRadius: '50%', marginRight: '10px' }} />
//               <p>{song.song_name}</p>
//               <button onClick={() => dispatch(setSelectedSong(song.song_id))}>Play</button>
//             </div>
//           ))}
//         </div>
//       )}

//       <div style={{ position: 'fixed', bottom: '0px', width: '100%', zIndex: '999', background: 'white' }}>
//         <div style={{ display: 'flex', alignItems: 'center' }}>
//           {currentSong && (
//             <div style={{ display: 'flex', alignItems: 'center', marginRight: '20px' }}>
//               <img src={currentSong.song_image} alt={currentSong.song_name} style={{ height: '40px', width: '40px', borderRadius: '50%', marginRight: '10px' }} />
//               <p>{currentSong.song_name}</p>
//             </div>
//           )}
//           <H5AudioPlayer
//             autoPlay
//             style={{ background: 'white' }}
//             src={currentSong ? currentSong.song : ''}
//             showJumpControls={false}
//             customAdditionalControls={[
//               <button key="previous" onClick={handlePrevious}>Previous</button>,
//               <button key="next" onClick={handleNext}>Next</button>,
//             ]}
//             ref={audioRef}
//           />
//           <button onClick={togglePlaylist}>{showPlaylist ? 'Hide Playlist' : 'Show Playlist'}</button>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default MusicPlayer;