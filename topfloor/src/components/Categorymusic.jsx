import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { VITE_API_BASE_URL } from '../api/env';
import MusicPlayer from './MusicPlayer';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedSong, setSongs } from '../redux/MusicSlice';
import ToastNotification from './ToastNotification';
import categoryMusicAPI from '../services/homeServices'
import playBtn from '../assets/images/play-icon2.png'

export default function Categorymusic() {
  const { id } = useParams();
  const [music, setMusic] = useState([]);
  const [existingPlaylists, setExistingPlaylists] = useState([]);
  const [notification, setNotification] = useState(null);
  const [showPlaylists, setShowPlaylists] = useState(""); // State for the selected song ID
  const [newPlaylistName, setNewPlaylistName] = useState(""); // State for new playlist name
  const dispatch = useDispatch();

  useEffect(() => {

    window.scrollTo(0, 0);

}, []);

  const handleImageClick = (index) => {
    dispatch(setSelectedSong(index));
    dispatch(setSongs(music));
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.post(`${VITE_API_BASE_URL}/songs`, {
          user_id: localStorage.getItem('userId'),
          songcategory_id: id
        });
        setMusic(response.data.data);
      } catch (error) {
        console.error('Error fetching song data:', error);
      }
    };
    fetchData();
  }, [id]);

  useEffect(() => {
    const fetchPlaylists = async () => {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');
      try {
        const response = await axios.post(`${VITE_API_BASE_URL}/playlist`, {
          user_id: userId,
        }, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
          },
        });
        if (response.status === 200) {
          setExistingPlaylists(response.data.data); // Assuming playlists are in the data field
        } else {
          console.error('Error fetching playlists');
        }
      } catch (error) {
        console.error('Error fetching playlists');
      }
    };
    fetchPlaylists();
  }, []);

  const handleAddFavorite = async (song_id) => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        console.error('User ID or token not found in local storage');
        return;
      }

      const response = await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
        user_id: userId,
        song_id: song_id,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
          'Authorization': `Bearer ${token}`,
        },
      });

      const updatedMusic = music.map(song =>
        song.song_id === song_id ? { ...song, favourites_status: response.data.favourites_status } : song
      );
      setMusic(updatedMusic);

      const notificationMessage = response.data.favourites_status ? 'Song Added to favorites' : 'Song Removed from favorites';
      setNotification(notificationMessage);
      setTimeout(() => setNotification(null), 3000);
    } catch (error) {
      console.error('Error adding to favorites:', error);
    }
  };

  // const handleAddFavorite = async (song_id) => {
  //     try {
  //         const userId = localStorage.getItem('userId');
  //         const token = localStorage.getItem('token');

  //         if (!userId || !token) {
  //             console.error('User ID or token not found in local storage');
  //             return;
  //         }

  //         await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
  //             user_id: userId,
  //             song_id: song_id,
  //         }, {
  //             headers: {
  //                 'Content-Type': 'application/json',
  //                 'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
  //                 'Authorization': `Bearer ${token}`,
  //             },
  //         });
  //         setNotification('Song Added To Favorite');
  //         setTimeout(() => setNotification(null), 3000);
  //     } catch (error) {
  //         console.error('Error adding to favorites:', error);
  //     }
  // };

  const handleAddToPlaylist = (songId) => {
    setShowPlaylists(songId); // Set the selected song ID
  };

  const addToPlaylist = async (songId, playlistId) => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        console.error('User ID or token not found in local storage');
        return;
      }
      console.log(userId, songId, playlistId)
      await axios.post(`${VITE_API_BASE_URL}/playlistsong/add`, {
        user_id: userId,
        song_id: songId,
        playlist_id: playlistId,

      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
          'Authorization': `Bearer ${token}`,
        },
      });
      console.log('Song added to playlist successfully');
      setNotification('Song Added to Playlist SuccessFully');
      setTimeout(() => setNotification(null), 3000);
      setShowPlaylists("");
    } catch (error) {
      console.error('Error adding song to playlist:', error);
    }
  };

  const createNewPlaylist = async (songId, playlistName) => {
    try {
      const userId = localStorage.getItem('userId');
      const response = await axios.post(`${VITE_API_BASE_URL}/playlist/add`, {
        user_id: userId,
        name: playlistName,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
        },
      });
      if (response.status === 200) {
        // Add the new playlist to existingPlaylists state
        setExistingPlaylists(prevPlaylists => [...prevPlaylists, response.data]);
        // Add the song to the newly created playlist
        await addToPlaylist(songId, response.data.playlist_id);
        setNewPlaylistName(""); // Clear the input field
        setNotification('Playlist Added Successfully and Song Added to Playlist');
        setTimeout(() => setNotification(null), 3000);
        setShowPlaylists("");
      }
    } catch (error) {
      console.error('Error creating or getting playlist:', error);
    }
  };

  return (

    <div>

      {notification && (
        <ToastNotification message={notification} duration={3000} />
      )}

      <div className={showPlaylists ? "playlist-section" : ""}>

        {music.map((song, index) => (
          <div key={index}>
            {showPlaylists === song.song_id && (
              <div className='modal-content'>
                <button className="btn btn-danger event-cross-btn" onClick={() => setShowPlaylists("")}>X</button>
                <div class="modal-icon"><i class="fas fa-music" aria-hidden="true"></i></div>


                <form>
                  <div className="from-group">
                    <input type="text" value={newPlaylistName} onChange={(e) => setNewPlaylistName(e.target.value)} placeholder="Enter new playlist name" className="from-control white" />
                    <div className="form-submit">
                      <button onClick={() => createNewPlaylist(song.song_id, newPlaylistName)} type="submit" className=" submit-playlist"> Submit </button>
                    </div>
                  </div>
                </form>
                <div class="playlist-box mt-4">
                  <ul>
                    {existingPlaylists.map(playlist => (

                      <li key={playlist.playlist_id}>
                        <a href="#" onClick={() => addToPlaylist(song.song_id, playlist.playlist_id)} class="playlist-box-text">{playlist.name}</a>
                      </li>

                    ))}
                  </ul>
                </div>
              </div>
            )}
          </div>
        ))}

      </div>


      <div className="main-card-container">

        <div className="card-container">
          {music.map((song, index) => (
            <div class="card">
              <div class="card-image">
                <img
                  src={song.song_image}
                  alt="Album Art"
                  height={200}
                  width={150}
                  onClick={() => handleImageClick(song.song_id)}
                  style={{ cursor: 'pointer' }}
                />
                <div class="card-overlay" onClick={() => handleImageClick(song.song_id)}>
                <div className='plyBtn'>
                    <span><img src={playBtn} alt="" /></span>
                  </div>
                  <div class="more-options">
                    <div class="dots">⁞</div>
                    <div class="dropdown">

                      <button className='btn-drop' onClick={() => handleAddToPlaylist(song.song_id)}>Add to Playlist</button>

                      <button className='btn-drop' onClick={() => handleAddFavorite(song.song_id)}>Add to Favorites</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-content">
                <p className="card-title">{song.song_name}</p>
                <p className="card-artist">{song.song_artist}</p>

              </div>
            </div>
          ))}
        </div>
      </div>

    </div>


    // <div>
    //     {notification && (
    //         <ToastNotification message={notification} duration={3000} />
    //     )}

    //     <div className={showPlaylists ? "playlist-section" : ""}>
    //                 <div className="card">
    //                   <div className="card-body">
    //                     {music.map((song, index) => (
    //                       <div key={index}>
    //                         {showPlaylists === song.song_id && (
    //                           <div>
    //                             <button className="btn btn-sm btn-secondary mb-2" onClick={() => setShowPlaylists("")}>Hide</button>
    //                             <h2>Choose Playlist:</h2>
    //                             {existingPlaylists.map(playlist => (
    //                               <div key={playlist.playlist_id}>
    //                                 <button onClick={() => addToPlaylist(song.song_id, playlist.playlist_id)}>{playlist.name}</button>
    //                               </div>
    //                             ))}
    //                             <input
    //                               type="text"
    //                               value={newPlaylistName}
    //                               onChange={(e) => setNewPlaylistName(e.target.value)}
    //                               placeholder="Enter new playlist name"
    //                             />
    //                             <button onClick={() => createNewPlaylist(song.song_id, newPlaylistName)}>Create Playlist</button>
    //                           </div>
    //                         )}
    //                       </div>
    //                     ))}
    //                   </div>
    //                 </div>
    //               </div>


    //     <div className='container'>
    //         <h1>Music</h1>
    //         <div className="row">
    //             {music.map((song, index) => (
    //                 <div className="col-md-4 mb-2" key={index}>
    //                     <div className="card">
    //                         <img
    //                             src={song.song_image}
    //                             className="card-img-top"
    //                             alt="Song Cover"
    //                             height={200}
    //                             width={150}
    //                             onClick={() => handleImageClick(song.song_id)}
    //                             style={{ cursor: 'pointer' }}
    //                         />
    //                         <div className="card-body">
    //                             <h5 className="card-title">{song.song_name}</h5>
    //                             <p className="card-text">{song.song_artist}</p>
    //                             <div>
    //                                 <button
    //                                     className="btn btn-success me-2"
    //                                     onClick={() => handleAddToPlaylist(song.song_id)} 
    //                                 >
    //                                     Add to Playlist
    //                                 </button>
    //                                 {/* {showPlaylists === song.song_id && ( 
    //                                     <div>
    //                                         <h2>Choose Playlist:</h2>
    //                                         {existingPlaylists.map(playlist => (
    //                                             <div key={playlist.playlist_id}>
    //                                                 <button onClick={() => addToPlaylist(song.song_id, playlist.playlist_id)}>{playlist.name}</button>
    //                                             </div>
    //                                         ))}
    //                                         <input
    //                                             type="text"
    //                                             value={newPlaylistName}
    //                                             onChange={(e) => setNewPlaylistName(e.target.value)}
    //                                             placeholder="Enter new playlist name"
    //                                         />
    //                                         <button onClick={() => createNewPlaylist(song.song_id, newPlaylistName)}>Create Playlist</button>
    //                                     </div>
    //                                 )} */}
    //                                 {/* <button
    //                                     className="btn btn-success my-2"
    //                                     onClick={() => handleAddFavorite(song.song_id)}
    //                                 >
    //                                     Add to Favorites
    //                                 </button> */}
    //                                 <button 
    //                                 className="btn btn-success my-2" 
    //                                 onClick={() => handleAddFavorite(song.song_id)}
    //                                 >
    //                                 {song.favourites_status ? "Remove from Favorites" : "Add to Favorites"}
    //                                 </button>
    //                                 </div>
    //                         </div>
    //                     </div>
    //                 </div>
    //             ))}
    //         </div>
    //     </div>
    // </div>
  );
}
