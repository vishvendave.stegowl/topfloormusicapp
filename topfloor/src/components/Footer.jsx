// import axios from 'axios';
// import React, { useEffect, useState } from 'react';
// import { Link } from 'react-router-dom';
// import Information from './Information';

// export default function Footer() {
//     const [footerData, setFooterData] = useState(null);

//     useEffect(() => {
//         const fetchData = async () => {
//             try {
//                 const FooterResponse = await axios.get('https://topfloormusicapp.com/auth/api/cms');
//                 const data = FooterResponse.data;
//                 setFooterData(data);
//             } catch (error) {
//                 console.error('Error fetching data:', error);
//             }
//         };
//         fetchData();
//     }, []);

//     return (
//         <div><br/>
//             <hr />
//             <Information/>
//             {footerData && (
//                 <div>
//                     <center>
//                     {footerData.logo_data.map((logo, index) => (
//                         <div key={index}>
//                             <img src={logo.footer_bg} height={150} width={150} alt={`Footer Logo ${index}`} />
//                             <h3>Download The App</h3>
//                             <p>Now Available On Google Play And The Apple Store.</p>
//                             <Link to={footerData.downloadourapp_data[index].android_link}>Android Link: </Link> &nbsp;
//                             <Link to={footerData.downloadourapp_data[index].ios_link}>iOS Link: </Link> &nbsp;
//                             <Link to= {footerData.downloadourapp_data[index].durisimo_link}>Durisimo Link:</Link> &nbsp;


//                             <h3>Social Media</h3>
//                             <Link to={footerData.socialmedia_data[index].facebook_link}>Facebook Link: </Link> &nbsp;
//                             <Link to={footerData.socialmedia_data[index].instagram_link}>Instagram Link:</Link> &nbsp;
//                             <Link to={footerData.socialmedia_data[index].twitter_link}>Twitter Link: </Link> &nbsp;
//                             <Link to={footerData.socialmedia_data[index].youtube_link}>Youtube Link: </Link> &nbsp;
//                             <hr />
//                             <p>{footerData.copyright_data[index].text}</p>
//                         </div>
//                     ))}
//                     </center>
//                 </div>
//             )}
//         </div>
//     );
// }

import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { VITE_API_BASE_URL } from '../api/env';
import '@fortawesome/fontawesome-free/css/all.min.css';
import googlePlayImg from '../assets/images/Google-play.png'
import appStoreImg from '../assets/images/App-store-Icons.png'
import durisimoAppStoreImg from '../assets/images/Durisimo-app-store.png'

export default function Footer() {
    const [footerLogo, setFooterLogo] = useState([])
    const [footerApp, setFootersApp] = useState([]);
    const [footerSM, setFootersSM] = useState([]);
    const [footerCR, setFootersCR] = useState([]);
    const [footerCU, setFooterCU] = useState([])
    const [bannerUrl, setBannerUrl] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            try {
                const FooterResponse = await axios.get(`${VITE_API_BASE_URL}/cms`);
                const downloadData = FooterResponse.data.downloadourapp_data;
                const socialMData = FooterResponse.data.socialmedia_data
                const footerLogoData = FooterResponse.data.logo_data
                const CopyRightData = FooterResponse.data.copyright_data
                const informationData = FooterResponse.data.contactus_data
                setFootersCR(CopyRightData);
                setFootersApp(downloadData);
                setFootersSM(socialMData);
                setFooterLogo(footerLogoData);
                setFooterCU(informationData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, []);


    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://topfloormusicapp.com/auth/api/booking');
                const data = await response.json();
                if (response.ok) {
                    setBannerUrl(data.data);
                } else {
                    // Handle error
                    console.error('Failed to fetch data:', data.message);
                }
            } catch (error) {
                // Handle network error
                console.error('Failed to fetch data:', error);
            }
        };

        fetchData();
    }, []);

    return (
        <div className='bg-grey'>
            <hr />
            <center>
                {
                    footerLogo.map((footerL, index) => (
                        <div className='footer-logo-container' key={index}>
                            <img src={footerL.footer_bg} height={150} width={150} />

                        </div>
                    ))
                }
            </center>
            <center>
                <div style={{ display: 'flex', gap: 70, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', flexWrap: 'wrap' }}>
                    {
                        footerCU.map((footercu, index) => (

                            <div key={index}>
                                <h3 className='red'> Info </h3>
                                <Link className='footer-link' target="blank" to="mailto:booktopfloor@icloud.com"><span><i className="fas fa-envelope" aria-hidden="true"></i></span><p>booktopfloor@icloud.com</p></Link>
                                <Link className='footer-link' target="blank" to="mailto:booktopfloor@icloud.com"><span><i className="fas fa-globe" aria-hidden="true"></i></span><p>topfloormusicapp.com</p></Link>
                                <Link className='footer-link' target="blank" to="mailto:booktopfloor@icloud.com"><span><i className="fas fa-phone-alt" aria-hidden="true"></i></span><p>Call Or text 917-682-2424</p></Link>
                                <Link className='footer-link' target="blank" to="mailto:booktopfloor@icloud.com"><span><i className="fas fa-map-marker-alt" aria-hidden="true"></i></span><p>World Wide Dj</p></Link>

                            </div>
                        ))
                    }
                    <div style={{ display: 'flex', gap: 70, flexDirection: 'column' }}>
                        {

                            footerApp.map((footersD, index) => (
                                <div key={index}>
                                    <h3 className='red'>Download The App</h3>
                                    <p className='red' style={{ fontSize: "10px" }} >Now Available On Google Play And The Apple Store.</p>
                                    <ul className="d-flex justify-content-center list-unstyled">
                                        <li className='mx-2'>
                                            <a title="Play Store" target="_blank" href="https://android.com">
                                                <img className="img-box" src={googlePlayImg} alt="Google Play Store" />
                                            </a>
                                        </li>
                                        <li>
                                            <a title="App Store" target="_blank" href="https://ios.com">
                                                <img className="img-box" src={appStoreImg} alt="Apple App Store" />
                                            </a>
                                        </li>
                                        <li>
                                            <a title="App Store" target="_blank" href="https://www.durisimoweb.com">
                                                <img className="img-box" src={durisimoAppStoreImg} alt="Durisimo App Store" />
                                            </a>
                                        </li>
                                    </ul>

                                </div>

                            ))
                        }
                        {
                            footerSM.map((footersm, index) => (
                                <div key={index}>
                                    <h3 className='red'>Social Media </h3>
                                    <ul className='d-flex justify-content-evenly list-unstyled'>
                                        <li><Link target="_blank" to={footersm.facebook_link}><span className='circular-icon'><i className="fa-brands fa-square-facebook" style={{ color: 'blue', fontSize: '30px' }} ></i></span></Link> <br /></li>
                                        <li><Link target="_blank" to={footersm.instagram_link}><span className='circular-icon'><i className="fa-brands fa-square-instagram" style={{ color: 'red', fontSize: '30px' }}></i></span></Link> <br /></li>
                                        <li><Link target="_blank" to={footersm.twitter_link}><span className='circular-icon'><i className="fa-brands fa-square-twitter" style={{ color: 'blue', fontSize: '30px' }}></i></span></Link> <br /></li>
                                        <li><Link target="_blank" to={footersm.youtube_link}><span className='circular-icon'><i className="fa-brands fa-square-youtube" style={{ color: 'red', fontSize: '30px' }}></i></span></Link><br /></li>
                                    </ul>

                                </div>

                            ))

                        }
                    </div>

                    {/* <div className="App">
                        <h3>Iframes in React</h3>
                        <iframe src="https://platform.twitter.com/widgets/tweet_button.html"></iframe>
                    </div> */}

                    <div>
                        {bannerUrl ? (
                            <iframe src={bannerUrl} title="Sponsor Banner"></iframe>
                        ) : (
                            <p>Loading...</p>
                        )}
                    </div>






                </div>
            </center >
            <hr />
            <center>
                {
                    footerCR.map((footerc, index) => (
                        <div key={index}>
                            <p className='red'>{footerc.text}</p>
                        </div>
                    ))
                }
            </center>

        </div >
    )
}
