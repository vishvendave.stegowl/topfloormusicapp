import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { VITE_API_BASE_URL } from '../api/env';
import ToastNotification from './ToastNotification';
import logo from '../assets/images/logo.png'
import playlistAPI from '../services/homeServices'

const PlaylistsPage = () => {
  const [playlists, setPlaylists] = useState([]);
  const [notification, setNotification] = useState(null);
  const navigate = useNavigate();
  const userId = localStorage.getItem('userId');

  useEffect(() => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        navigate('/');
      }
    } catch (error) {
      console.error('Error accessing local storage:', error);
    }
  }, []);

  useEffect(() => {
    playlistAPI.fetchPlaylists().then((res)=>{
      console.log(res.data)
      setPlaylists(res.data);
    })
    // fetchPlaylists();
  }, []);

  // const fetchPlaylists = async () => {
  //   try {
  //     const response = await axios.post(`${VITE_API_BASE_URL}/playlist`, {
  //       user_id: userId,
  //     }, {
  //       headers: {
  //         'Content-Type': 'application/json',
  //         // 'X-CSRFToken': 'k', // You can include the CSRF token if necessary
  //       },
  //     });
  //     if (response.status === 200) {
  //       console.log(response.data.data)
  //       setPlaylists(response.data.data); // Assuming playlists are in the data field
  //     } else {
  //       console.error('Error fetching playlists');
  //     }
  //   } catch (error) {
  //     console.error('Error fetching playlists');
  //   }
  // };

  const removePlaylist = async (playlistId) => {
    const confirmation = window.confirm('Are you sure you want to remove this playlist?');
    if (!confirmation) {
      return; // If the user cancels, do nothing
    }
    try {
      const response = await axios.post(`${VITE_API_BASE_URL}/playlist/remove`, {
        user_id: userId,
        playlist_id: playlistId,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'koAiIX92tSusWT1jFeCxWimiWsFMpRsToNK9UtsDbcnZihlIE26evHWx0T34tzng',
        },
      });
      if (response.status === 200) {
        // Filter out the removed playlist from the state
        setPlaylists(prevPlaylists => prevPlaylists.filter(playlist => playlist.playlist_id !== playlistId));
        setNotification('PlayList Removed Successfully');
        setTimeout(() => setNotification(null), 3000);
        console.log("playlist removed successfully")
      } else {
        console.error('Error removing playlist');
      }
    } catch (error) {
      console.error('Error removing playlist');
    }
  };

  return (
    <>

      {notification && (
        <ToastNotification message={notification} duration={3000} />
      )}
      <div className="container">

        <h2 className="showtime-heading">My Playlists</h2>
        <div className="row">
          {playlists.length > 0 ?
            playlists.map((playlist) => (

              <div className="col-md-2 mb-2" key={playlist.playlist_id}>
                <div className="card">
                  {console.log(playlist)}
                  <Link to={`/playlist/${playlist.playlist_id}`} className="btn ">
                    <img
                      src={logo}
                      alt="playist"
                      height={200}
                      width={150}
                      // onClick={() => handleImageClick(song)}
                      style={{ cursor: 'pointer' }}
                    /></Link>
                  <div className="card-body">

                    <p className='red'>{playlist.name}</p>

                  </div>
                </div>
              </div>
            )) :                
            <div className="empty">
            <div className="inner-empty">
              <p>❤</p>
             <h2>No favorite items found</h2>
            </div>
            
          </div>}

        </div>
      </div>

    </>
  );
};

export default PlaylistsPage;

