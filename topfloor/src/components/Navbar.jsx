import React from 'react'
import { Link } from 'react-router-dom'
import logo from '../assets/images/Logo_Transperent.png';
import { NavLink } from 'react-router-dom';
import '../css/Nav.css'

export default function Navbar() {
  const toggleNavbar = () => {
    const navLinks = document.querySelector('.nav-links');
    navLinks.classList.toggle('show');
  };



  const handleNavLinkClick = () => {
    const navLinks = document.querySelector('.nav-links');
    if (navLinks.classList.contains('show')) {
      navLinks.classList.remove('show');
    }
  };
  
  return (
    // <div>
    //   <div id='header'>
    //     <strong>
    //       <nav className="theme-dark-head fixed-top " >
    //         <div className='nav-container'>
    //           <div className="row-content">
    //             <div className="logo-nav">
    //               <div className="logoo">
    //                 <a href="#"><img src={logo} alt="" /></a>
    //               </div>

    //             </div>
    //             <div className="routes-nav">

    //             </div>

    //           </div>
    //         </div>

    //       </nav>
    //     </strong>
    //   </div>
    // </div>
    
  //   <div>
  //   <div id='header'>
  //     <strong>
  //     <nav className="theme-dark-head container fixed-top " >
  //     <div className='nav-container'>
  //           <div className='row-content container-fluid'>
  //           <Link className="navbar-brand logo" to="/">
  //             <img src={logo} alt="Logo" width="90" height="90" className="d-flex justify-content-around" />
  //           </Link>
  //           {/* <Link className="navbar-brand" to="/">Navbar</Link> */}
           
  //           <button className="navbar-toggler"  >
  //             <span ><i class="fas fa-search red" aria-hidden="true"></i></span>
  //           </button>
  //           <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  //             <span ><i class="fas fa-bars red" aria-hidden="true"></i></span>
  //           </button>
  //           <div className="collapse navbar-collapse" id="navbarSupportedContent">
  //             <ul className="navbar-nav me-auto mb-2 mb-lg-0">

  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/home"><i className="fa fa-home p-2" aria-hidden="true"></i>Home</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/music"><i className="fa fa-music p-2" aria-hidden="true"></i>Music</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/radio"><i className="fa fa-microphone p-2" aria-hidden="true"></i>Radio</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/showtime"><i className="fa fa-eye p-2" aria-hidden="true"></i>Showtime</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/livestream"><i className="fa fa-video-camera p-2" aria-hidden="true"></i>LiveStream</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/events"><i className="fa fa-calendar-o p-2" aria-hidden="true"></i>Events</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/myplaylist"><i className="fa fa-play-circle-o p-2" aria-hidden="true"></i>My PlayList</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/favorites"><i className="fa fa-heart p-2" aria-hidden="true"></i>Favorites</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/info"><i className="fa fa-envelope p-2" aria-hidden="true"></i>Info</NavLink>
  //               </li>
  //               <li className="nav-item">
  //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/search"><i className="fa fa-search p-2" aria-hidden="true"></i></NavLink>
  //               </li>


  //               {/* <li className="nav-item">
  //                 <Link className="nav-link disabled" aria-disabled="true">Disabled</Link>
  //                 </li> */}
  //             </ul>

  //           </div>

  //           </div>
  //         </div>
  //       </nav>
  //     </strong>
  //   </div>
  // </div>




    // <div>
    //   <div id='header' className='nav-background'>
    //     <strong>
    //       {/* <nav className="navbar navbar-expand-lg navbar-bg-primary fixed-top " >
    //         <div className="container-fluid d-flex nav-background nav-inner" > */}
    //          <nav className="navbar navbar-expand-lg navbar-bg-primary fixed-top " >
    //            <div className="container-fluid d-flex  nav-inner container" >
    //           <Link className="navbar-brand logo" to="/">
    //             <img src={logo} alt="Logo" width="90" height="90" className="d-flex justify-content-around" />
    //           </Link>
    //           {/* <Link className="navbar-brand" to="/">Navbar</Link> */}
             
    //           <button className="navbar-toggler"  >
    //             <span ><i class="fas fa-search red" aria-hidden="true"></i></span>
    //           </button>
    //           <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    //             <span ><i class="fas fa-bars red" aria-hidden="true"></i></span>
    //           </button>
    //           <div className="collapse navbar-collapse" id="navbarSupportedContent">
    //             <ul className="navbar-nav me-auto mb-2 mb-lg-0">

    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/home"><i className="fa fa-home p-2" aria-hidden="true"></i>Home</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/music"><i className="fa fa-music p-2" aria-hidden="true"></i>Music</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/radio"><i className="fa fa-microphone p-2" aria-hidden="true"></i>Radio</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/showtime"><i className="fa fa-eye p-2" aria-hidden="true"></i>Showtime</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/livestream"><i className="fa fa-video-camera p-2" aria-hidden="true"></i>LiveStream</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/events"><i className="fa fa-calendar-o p-2" aria-hidden="true"></i>Events</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/myplaylist"><i className="fa fa-play-circle-o p-2" aria-hidden="true"></i>My PlayList</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/favorites"><i className="fa fa-heart p-2" aria-hidden="true"></i>Favorites</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/info"><i className="fa fa-envelope p-2" aria-hidden="true"></i>Info</NavLink>
    //               </li>
    //               <li className="nav-item">
    //                 <NavLink className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} to="/search"><i className="fa fa-search p-2" aria-hidden="true"></i></NavLink>
    //               </li>


    //               {/* <li className="nav-item">
    //                 <Link className="nav-link disabled" aria-disabled="true">Disabled</Link>
    //                 </li> */}
    //             </ul>

    //           </div>
    //         </div>
    //       </nav>
    //     </strong>
    //   </div>
    // </div>



   <div>
      <div id='header' className='nav-background'>
    <nav>
    <div className="container">
      <Link to="/" className="logo" id='logo-navi'>
        <img src={logo} alt="Logo"  style={{height:'90px',width:'90px'}}  />
      </Link>
      <button className="navbar-toggler" onClick={toggleNavbar}>
        <i className="fas fa-bars"></i>
      </button>
      <ul className="nav-links">
        <li>
          <NavLink to="/home"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")}  onClick={handleNavLinkClick}>
            <i className="fa fa-home nav-icons "></i><span className='nav-text nav-icons'>Home</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/music"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-music nav-icons" ></i><span className='nav-text'>Music</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/radio"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-microphone nav-icons" ></i><span className='nav-text'>Radio</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/showtime"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-eye nav-icons"  ></i><span className='nav-text'>Showtime</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/livestream"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-video-camera nav-icons" ></i><span className='nav-text'>LiveStream</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/events"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-calendar-o nav-icons" ></i><span className='nav-text'>Events</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/myplaylist"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-play-circle-o nav-icons" ></i><span className='nav-text'>My PlayList</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/favorites"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-heart nav-icons" ></i><span className='nav-text'>Favorites</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/info"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-envelope nav-icons" ></i><span className='nav-text'>Info</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/search"  className={({ isActive }) => (isActive ? "nav-link active my-active-link" : "nav-link")} onClick={handleNavLinkClick}>
            <i className="fa fa-search nav-icons" ></i>
          </NavLink>
        </li>
      </ul>
    </div>
  </nav>
  </div>
  </div>
  )
}









// import React from 'react'
// import { Link } from 'react-router-dom'
// import logo from '../assets/images/logo.png';

// export default function Navbar() {
//   return (
//     <div>
//       <strong>
//             <nav className="navbar navbar-expand-lg bg-primary fixed-top ">
//             <div className="container-fluid">
//                 <Link className="navbar-brand" to="/">
//                 <img src={logo} alt="Logo" width="30" height="30" className="d-inline-block align-top" />
//                 </Link>
//                 {/* <Link className="navbar-brand" to="/">Navbar</Link> */}
//                 <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
//                 <span className="navbar-toggler-icon"></span>
//                 </button> 
//                 <div className="collapse navbar-collapse" id="navbarSupportedContent">
//                 <ul className="navbar-nav me-auto mb-2 mb-lg-0">
//                     <li className="nav-item">
//                     <Link className="nav-link active" aria-current="page" to="/home">Home</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/music">Music</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/radio">Radio</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/showtime">Showtime</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/livestream">LiveStream</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/events">Events</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/myplaylist">My PlayList</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/favorites">Favroites</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/info">Info</Link>
//                     </li>
//                     <li className="nav-item">
//                     <Link className="nav-link" to="/search">Search</Link>
//                     </li>
//                     {/* <li className="nav-item">
//                     <Link className="nav-link disabled" aria-disabled="true">Disabled</Link>
//                     </li> */}
//                 </ul>
                
//                 </div>
//             </div>
//             </nav>
//             </strong>
//     </div>
//   )
// }


