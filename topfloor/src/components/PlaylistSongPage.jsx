import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { VITE_API_BASE_URL } from '../api/env';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedSong, setSongs } from '../redux/MusicSlice';
import ToastNotification from './ToastNotification';


const PlaylistSongsPage = () => {
  const { playlistId } = useParams();
  const [songs, setSonges] = useState([]);  // PLEASE don't Correct the Spealing of setSonges, otherwise Music will not work
  const [loading, setLoading] = useState(true); 
  const userId = localStorage.getItem('userId');
  const [notification, setNotification] = useState(null);
  // const [selectedSongIndex, setSelectedSongIndex] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    fetchPlaylistSongs();
  }, []);

  // const handleImageClick = (index) => {
  //   dispatch(setSelectedSong(index));
  //   console.log(setSelectedSong(index.song_id));
  //   dispatch(setSongs(songs))
  // }; 

  const handleImageClick = (index) => {
    const selectedSong = songs[index]; 
    dispatch(setSelectedSong(selectedSong.song_id));
    dispatch(setSongs(songs)) // this setSongs is different of Redux setSongs
    console.log(selectedSong.song_id);
};

  const fetchPlaylistSongs = async () => {
    try {
      const response = await axios.post(`${VITE_API_BASE_URL}/playlistsong`, {
        user_id: userId,
        playlist_id: playlistId,
      }, {
        headers: {
          'Content-Type': 'application/json',
          // 'X-CSRFToken': 'aaa', 
        },
      });
      if (response.status === 200) {
        setSonges(response.data.data);
      } else {
        console.error('Error fetching playlist songs');
      }
    } catch (error) {
      console.error('Error fetching playlist songs');
    } finally {
      setLoading(false);
    }
  };

  const removeFromPlaylist = async (playlistsongId) => {
  const confirmation = window.confirm('Are you sure you want to delete this Song fom Playlist?');
    if (!confirmation) {
          return;
    }
    try {
      const response = await axios.post(`${VITE_API_BASE_URL}/playlistsong/remove`, {
        user_id: userId,
        playlistsong_id: playlistsongId,
      }, {
        headers: {
          'Content-Type': 'application/json',
          //   'X-CSRFToken': '',
        },
      });
      if (response.status === 200) {
        // Remove the song from the state
        setSonges(prevSongs => prevSongs.filter(song => song.playlistsong_id !== playlistsongId));
        setNotification('Song removed Successfully');
        setTimeout(() => setNotification(null), 3000);
      } else {
        console.error('Error removing song from playlist');
      }
    } catch (error) {
      console.error('Error removing song from playlist');
    }
  };

  return (
    <>
       {notification && ( 
        <ToastNotification message={notification} duration={3000} />
      )} 
      <div className="container">
        <h2 className="my-4">Playlist Songs</h2>
        {loading ? ( // Render the spinner if loading is true
          <div className="d-flex justify-content-center">
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        ) : (
          <div className="row">
            {songs.map((song, index) => (
              <div className="col-md-4 mb-3" key={song.playlistsong_id}>
                <div className="card">
                  <img src={song.song_image} onClick={() => handleImageClick(index)} className="card-img-top" alt={song.song_name} style={{cursor:'pointer'}} />
                  <div className="card-body">
                    <h5 className="card-title">{song.song_name}</h5>
                    <p className="card-text">Artist: {song.song_artist}</p>

           

                    <button onClick={() => removeFromPlaylist(song.playlistsong_id)} className="btn btn-danger">Remove from Playlist</button>
                  </div>
                </div>
              </div>
            ))}
            
          </div>
        )}

      </div>
    </>
  );
};

export default PlaylistSongsPage;
