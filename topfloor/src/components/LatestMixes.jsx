// import React, { useState, useEffect, useRef } from 'react';
// import axios from 'axios';
// import { VITE_API_BASE_URL } from '../api/env';
// import MusicPlayer from './MusicPlayer';

// const LatestMixes = () => {
//   const [music, setMusic] = useState([]);
//   const [selectedSongIndex, setSelectedSongIndex] = useState(null);


//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const LatestMixesData = await axios.get(`${VITE_API_BASE_URL}/cms`);
//         const songData = LatestMixesData.data.song_data;
//         setMusic(songData);
//       } catch (error) {
//         console.error('Error fetching image data:', error);
//       }
//     };
//     fetchData();
//   }, []);

//   const handleAddFavorite = async (song_id) => {
//     try {
//       const userId = localStorage.getItem('userId');
//       const token = localStorage.getItem('token');

//       if (!userId || !token) {
//         console.error('User ID or token not found in local storage');
//         return;
//       }

//       await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
//         user_id: userId,
//         song_id: song_id,
//       }, {
//         headers: {
//           'Content-Type': 'application/json',
//           'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
//           'Authorization': `Bearer ${token}`,
//         },
//       });
//       console.log('Song added to favorites');
//     } catch (error) {
//       console.error('Error adding to favorites:', error);
//     }
//   };

//   const handleSongClick = (index) => {
//     setSelectedSongIndex(index);
//   };

//   const handleNext = () => {
//     if (selectedSongIndex !== null && selectedSongIndex < music.length - 1) {
//       setSelectedSongIndex(selectedSongIndex + 1);

//     }
//   };

//   const handlePrevious = () => {
//     if (selectedSongIndex !== null && selectedSongIndex > 0) {
//       setSelectedSongIndex(selectedSongIndex - 1);

//     }
//   };

//   const handleSongStart = () => {
//     setShowPlaylistButton(true); 
//     setIsPlaylistPlaying(true); 
//   };

//   const handleAddToPlaylist = async (songId) => {
//     try {
//       const newPlaylistName = prompt('Enter the name of the playlist:');
//       if (!newPlaylistName) return;

//       const userId = localStorage.getItem('userId');
//       const token = localStorage.getItem('token');

//       if (!userId || !token) {
//         console.error('User ID or token not found in local storage');
//         return;
//       }

//       const playlistId = await createOrGetPlaylist(newPlaylistName);

//       await axios.post(`${VITE_API_BASE_URL}/playlistsong/add`, {
//         user_id: userId,
//         song_id: songId,
//         playlist_id: playlistId,
//       }, {
//         headers: {
//           'Content-Type': 'application/json',
//           'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
//           'Authorization': `Bearer ${token}`,
//         },
//       });
//       console.log('Song added to playlist successfully');
//     } catch (error) {
//       console.error('Error adding song to playlist:', error);
//     }
//   };

//   const createOrGetPlaylist = async (playlistName) => {
//     try {
//       const userId = localStorage.getItem('userId');
//       const response = await axios.post(`${VITE_API_BASE_URL}/playlist/add`, {
//         user_id: userId,
//         name: playlistName,
//       }, {
//         headers: {
//           'Content-Type': 'application/json',
//           'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
//         },
//       });
//       if (response.status === 200) {
//         return response.data.playlist_id;
//       }
//     } catch (error) {
//       console.error('Error creating or getting playlist:', error);
//       throw error;
//     }
//   };

//   return (
//     <div className="container">
//         <div className="row">
//             {music.map((song, index) => (
//                 <div key={`song-${index}`} className="col-md-4 mb-4">
//                     <div className="card">
//                         <img
//                             src={song.song_image}
//                             className="card-img-top"
//                             alt="Album Art"
//                             height={200}
//                             width={150}
//                             onClick={() => handleSongClick(index)}
//                             style={{ cursor: 'pointer' }}
//                         />
//                         <div className="card-body" >
//                             <h5 className="card-title">{song.song_name}</h5>
//                             <p className="card-text">{song.song_artist}</p>
//                             <button className="btn btn-success mx-2 my-2" onClick={() => handleAddToPlaylist(song.song_id)}>Add to Playlist</button>
//                             <button className="btn btn-success" onClick={() => handleAddFavorite(song.song_id)}>Add to Favorites</button>
//                         </div>
//                     </div>
//                 </div>
//             ))}
//         </div>
//         <div className="row">
//             <div className="col">
//                 {selectedSongIndex !== null && (
//                     <MusicPlayer
//                         playlist={music}
//                         currentSongIndex={selectedSongIndex}
//                         onNext={handleNext}
//                         onPrevious={handlePrevious}
//                         onSongStart={handleSongStart}
//                     />
//                 )}
//             </div>
//         </div>
//     </div>
// );
// };

// export default LatestMixes;








import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import homeAPI from '../services/homeServices'
import { VITE_API_BASE_URL } from '../api/env';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedSong } from '../redux/MusicSlice';
import { setSongs } from '../redux/MusicSlice';
import ToastNotification from './ToastNotification';
import playBtn from '../assets/images/play-icon2.png'

const LatestMixes = () => {
  const dispatch = useDispatch();
  const [music, setMusic] = useState([]);
  const [existingPlaylists, setExistingPlaylists] = useState([]);
  const [notification, setNotification] = useState(null);
  const [showPlaylists, setShowPlaylists] = useState(""); // State for the selected song ID
  const [newPlaylistName, setNewPlaylistName] = useState("");

  useEffect(() => {
    homeAPI.getHomeData().then((res)=>{
      const songData = res.song_data;
      setMusic(songData);

    })
    // const fetchData = async () => {
    //   try {
    //     const LatestMixesData = await axios.get(`${VITE_API_BASE_URL}/cms`);
    //     const songData = LatestMixesData.data.song_data;
    //     setMusic(songData);
    //   } catch (error) {
    //     console.error('Error fetching image data:', error);
    //   }
    // };
    // fetchData();
  }, []);

  useEffect(() => {
    homeAPI.fetchPlaylists().then((res)=>{
     setExistingPlaylists(res.data); // Assuming playlists are in the data field
            
    }).catch((error)=>{
      console.log(error)

    })
    // const fetchPlaylists = async () => {
    //   const userId = localStorage.getItem('userId');
    //   const token = localStorage.getItem('token');
    //   try {
    //     const response = await axios.post(`${VITE_API_BASE_URL}/playlist`, {
    //       user_id: userId,
    //     }, {
    //       headers: {
    //         'Content-Type': 'application/json',
    //         'Authorization': `Bearer ${token}`,
    //         // 'X-CSRFToken': 'k', // You can include the CSRF token if necessary
    //       },
    //     });
    //     if (response.status === 200) {
    //       setExistingPlaylists(response.data.data); // Assuming playlists are in the data field
    //     } else {
    //       console.error('Error fetching playlists');
    //     }
    //   } catch (error) {
    //     console.error('Error fetching playlists');
    //   }
    // };
    // fetchPlaylists();
  }, []);


  const [favoriteItems, setFavoriteItems] = useState([]);

  useEffect(() => {
    homeAPI.fetchFavorites().then((res)=>{
      if (res && Array.isArray(res.data)) {
        setFavoriteItems(res.data);
      } else {
        console.error('Array of favorite items not found in response:', res.data);
      }
    })
    // const fetchFavorites = async () => {
    //   try {
    //     const userId = localStorage.getItem('userId');
    //     const token = localStorage.getItem('token');

    //     if (!userId || !token) {
    //       console.error('User ID or token not found in local storage');
    //       return;
    //     }

    //     const response = await axios.post(`${VITE_API_BASE_URL}/favourites`, { user_id: userId }, {
    //       headers: {
    //         'Content-Type': 'application/json',
    //         'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
    //         'Authorization': `Bearer ${token}`,
    //       }
    //     });

    //     if (response.data && Array.isArray(response.data.data)) {
    //       setFavoriteItems(response.data.data);
    //     } else {
    //       console.error('Array of favorite items not found in response:', response.data);
    //     }
    //   } catch (error) {
    //     console.error('Error fetching favorite items:', error);
    //   }
    // };

    // fetchFavorites();
  }, []);

  const isSongFavorited = (songId) => {
    return favoriteItems.some(item => item.song_id === songId);
  };

  // const handleAddFavorite = async (song_id) => {
  //   try {
  //     const userId = localStorage.getItem('userId');
  //     const token = localStorage.getItem('token');

  //     if (!userId || !token) {
  //       console.error('User ID or token not found in local storage');
  //       return;
  //     }

  //     const response = await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
  //       user_id: userId,
  //       song_id: song_id,
  //     }, {
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
  //         'Authorization': `Bearer ${token}`,
  //       },
  //     });

  //     const updatedMusic = music.map(song =>
  //       song.song_id === song_id ? { ...song, favourites_status: response.data.favourites_status } : song
  //     );
  //     setMusic(updatedMusic);

  //     const notificationMessage = response.data.favourites_status ? 'Song Added to favorites' : 'Song Removed from favorites';
  //     setNotification(notificationMessage);
  //     setTimeout(() => setNotification(null), 3000);
  //   } catch (error) {
  //     console.error('Error adding to favorites:', error);
  //   }
  // };

  const handleAddFavorite = async (song_id) => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        console.error('User ID or token not found in local storage');
        return;
      }

      const response = await axios.post(`${VITE_API_BASE_URL}/favourites/add_remove`, {
        user_id: userId,
        song_id: song_id,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
          'Authorization': `Bearer ${token}`,
        },
      });

      const isAddedToFavorites = response.data.favourites_status;
      const updatedMusic = music.map(song =>
        song.song_id === song_id ? { ...song, favorite: isAddedToFavorites } : song
      );
      setMusic(updatedMusic);
      const notificationMessage = isAddedToFavorites ? 'Song Added to favorites' : 'Song Removed from favorites';

      setNotification(notificationMessage);
      setTimeout(() => setNotification(null), 3000);

    } catch (error) {
      console.error('Error adding to favorites:', error);
    }
  };

  const handleImageClick = (index) => {
    dispatch(setSelectedSong(index.song_id));
    console.log(setSelectedSong(index.song_id));
    dispatch(setSongs(music)) // Assuming index is the song object
  };

  const handleAddToPlaylist = (songId) => {
    setShowPlaylists(songId); // Set the selected song ID
  };

  const addToPlaylist = async (songId, playlistId) => {
    try {
      const userId = localStorage.getItem('userId');
      const token = localStorage.getItem('token');

      if (!userId || !token) {
        console.error('User ID or token not found in local storage');
        return;
      }

      await axios.post(`${VITE_API_BASE_URL}/playlistsong/add`, {
        user_id: userId,
        song_id: songId,
        playlist_id: playlistId,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
          'Authorization': `Bearer ${token}`,
        },
      });
      console.log('Song added to playlist successfully');
      setNotification('Song Added to Playlist SuccessFully');
      setTimeout(() => setNotification(null), 3000);
      setShowPlaylists("");
    } catch (error) {
      console.error('Error adding song to playlist:', error);
    }
  };

  const createNewPlaylist = async (songId, playlistName) => {
    try {
      const userId = localStorage.getItem('userId');
      const response = await axios.post(`${VITE_API_BASE_URL}/playlist/add`, {
        user_id: userId,
        name: playlistName,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': 'FIr8TV6m6ssUHRR92e34lTzpXTBAiSmzJ7BZ5rpXOMlr3fby12xLUi9E1kZSmAhW',
        },
      });
      if (response.status === 200) {
        // Add the new playlist to existingPlaylists state
        setExistingPlaylists(prevPlaylists => [...prevPlaylists, response.data]);
        // Add the song to the newly created playlist
        await addToPlaylist(songId, response.data.playlist_id);
        setNewPlaylistName(""); // Clear the input field
        setNotification('Playlist Added Successfully and Song Added to Playlist');
        setTimeout(() => setNotification(null), 3000);
        setShowPlaylists("");
      }
    } catch (error) {
      console.error('Error creating or getting playlist:', error);
    }
  };

  return (
    <>
      {notification && (
        <ToastNotification message={notification} duration={3000} />
      )}


      {/* <div class="main-mixes-container">
        <div class="card-mixes-container">
        {music.map((song, index) => (
            <div class="card">
                <div class="card-image">
                <img
                        src={song.song_image}
                        alt="Album Art"
                        height={200}
                        width={150}
                        onClick={() => handleImageClick(song)}
                        style={{ cursor: 'pointer' }} 
                      />
                    <div class="card-overlay">
                        <div class="more-options">
                            <div class="dots">•••</div>
                            <div class="dropdown">
                                <a href="#">Option 1</a>
                                <a href="#">Option 2</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                    <p class="card-title">24-Jul</p>
                    <p class="card-artist">Becky G</p>
                </div>
            </div>
            ))}
        </div>
    </div> */}

      <div className="main-card-container">

        <div className="card-container">
          {music.map((song, index) => (
            <div class="card">
              <div class="card-image">
                <img
                  src={song.song_image}
                  alt="Album Art"
                  height={200}
                  width={150}
                  onClick={() => handleImageClick(song)}
                  style={{ cursor: 'pointer' }}
                />
                <div class="card-overlay" onClick={() => handleImageClick(song)}>
                <div className='plyBtn'>
                    <span><img src={playBtn} alt="" /></span>
                  </div>
                  <div class="more-options">
                    <div class="dots">⁞</div>
                    <div class="dropdown">

                      <button className='btn-drop' onClick={() => handleAddToPlaylist(song.song_id)}>Add to Playlist</button>

                      <button className='btn-drop' onClick={() => handleAddFavorite(song.song_id)}>Add to Favorites</button>
                    </div>
                  </div>
                  
                </div>
                
              </div>
              <div class="card-content">
                <p className="card-title">{song.song_name}</p>
                <p className="card-artist">{song.song_artist}</p>

              </div>
            </div>
          ))}
        </div>
      </div>

      <div className={showPlaylists ? "playlist-section" : ""}>

        {music.map((song, index) => (
          <div key={index}>
            {showPlaylists === song.song_id && (
              <div className='modal-content'>
                <button className="btn btn-danger event-cross-btn" onClick={() => setShowPlaylists("")}>X</button>
                <div class="modal-icon"><i class="fas fa-music" aria-hidden="true"></i></div>

                <form>
                  <div className="from-group">
                    <input type="text" value={newPlaylistName} onChange={(e) => setNewPlaylistName(e.target.value)} placeholder="Enter new playlist name" className="from-control white" />
                    <div className="form-submit">
                      <button onClick={() => createNewPlaylist(song.song_id, newPlaylistName)} type="submit" className=" submit-playlist"> Submit </button>
                    </div>
                  </div>
                </form>
                <div class="playlist-box mt-4">
                  <ul>
                    {existingPlaylists.map(playlist => (

                      <li key={playlist.playlist_id}>
                        <a href="" onClick='{addToPlaylist(song.song_id, playlist.playlist_id)}' class="playlist-box-text">{playlist.name}</a>
                      </li>

                    ))}
                  </ul>
                </div>
              </div>
            )}
          </div>
        ))}

      </div>


      {/* <div className="container">
        <div className="row">
            {music.map((song, index) => (
                <div key={`song-${index}`} className="col-md-4 mb-4">
                    <div className="card">
                         <img
                        src={song.song_image}
                        alt="Album Art"
                        height={200}
                        width={150}
                        onClick={() => handleImageClick(song)}
                        style={{ cursor: 'pointer' }} 
                      />
                        <div className="card-body" >
                            <h5 className="card-title">{song.song_name}</h5>
                            <p className="card-text">{song.song_artist}</p>
                            <button className="btn btn-success mx-2 my-2" onClick={() => handleAddToPlaylist(song.song_id)}>Add to Playlist</button>
                            <button className="btn btn-success" onClick={() => handleAddFavorite(song.song_id)}>Add to Favorites</button>
                         
                      
                        </div>
                    </div>
                </div>
            ))}
        </div>


        <div className={showPlaylists ? "playlist-section" : ""}>
            <div className="card">
              <div className="card-body">
                {music.map((song, index) => (
                  <div key={index}>
                    {showPlaylists === song.song_id && (
                      <div>
                        <button className="btn btn-sm btn-secondary mb-2" onClick={() => setShowPlaylists("")}>Hide</button>
                        <h2>Choose Playlist:</h2>
                        {existingPlaylists.map(playlist => (
                          <div key={playlist.playlist_id}>
                            <button onClick={() => addToPlaylist(song.song_id, playlist.playlist_id)}>{playlist.name}</button>
                          </div>
                        ))}
                        <input
                          type="text"
                          value={newPlaylistName}
                          onChange={(e) => setNewPlaylistName(e.target.value)}
                          placeholder="Enter new playlist name"
                        />
                        <button onClick={() => createNewPlaylist(song.song_id, newPlaylistName)}>Create Playlist</button>
                      </div>
                    )}
                  </div>
                ))}
              </div>
            </div>
          </div>




    </div> */}
    </>
  );
};

export default LatestMixes;













