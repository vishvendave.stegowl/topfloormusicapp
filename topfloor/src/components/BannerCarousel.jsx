import React from 'react'
import Autoplay from 'embla-carousel-autoplay'
import useEmblaCarousel from 'embla-carousel-react'

const BannerCarousel = (props) => {
  const { slides, options } = props
  const [emblaRef, emblaApi] = useEmblaCarousel(options, [Autoplay()])

  return (
    <section className="embla">
      <div className="embla__viewport" ref={emblaRef}>
        <div className="embla__container">
          {slides.map((index) => (
            <div className="embla__slide" key={index}>
              <div className="embla__slide__number">
                {/* <img src={index.image} className="bannerIMg d-block w-100" /> */}
                <img src={index.image} className="bannerIMg " style={{width:'100%'}} />
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  )
}

export default BannerCarousel
