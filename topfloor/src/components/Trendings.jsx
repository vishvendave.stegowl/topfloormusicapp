import axios from 'axios';
import homeAPI from '../services/homeServices'
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { VITE_API_BASE_URL } from '../api/env';

export default function Trendings() {
    const [trendings, setTrendings] = useState([]);
    console.log("trendings", trendings)
    useEffect(() => {
    homeAPI.getHomeData().then((res)=>{
        const imageData = res.songcategory_data;
         setTrendings(imageData);

    })
        // const fetchData = async () => {
        //     try {
        //         const imageDataResponse = await axios.get(`${VITE_API_BASE_URL}/cms`);

        //         const imageData = imageDataResponse.data.songcategory_data;
        //         setTrendings(imageData);
        //     } catch (error) {
        //         console.error('Error fetching image data:', error);
        //     }
        // };
        // fetchData();
    }, []);

    return (
        <>
            <div className="main-card-container">
                <div className="card-container">
                    {trendings.map((banner, index) => (
                        <div className="card" key={`banner-${index}`} >
                            <Link to={`/categorymusic/${banner.songcategory_id}`} style={{ textDecoration: 'none', color: 'red' }}>
                                <img src={banner.image} className="card-img-top" alt="Banner" height={200} width={150} />
                                <p>{banner.name}</p>
                            </Link>
                        </div>
                    ))}
                </div>
            </div>
        </>


        // <div className="container">
        //     <div className="row">
        //         {trendings.map((banner, index) => (
        //             <div key={`banner-${index}`} className="col-md-4 mb-4">
        //                 <div className="card">
        //                     <Link to={`/categorymusic/${banner.songcategory_id}`}>
        //                         <img src={banner.image} className="card-img-top" alt="Banner" height={200} width={150} />
        //                         <div className="card-body">
        //                             <h5 className="card-title">{banner.name}</h5>
        //                         </div>
        //                     </Link>
        //                 </div>
        //             </div>
        //         ))}
        //     </div>
        // </div>
    );
}



