import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { VITE_API_BASE_URL } from '../api/env';

function Notification() {
    const [notification, setNotification] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            try {
                const notificationResponse = await axios.get(`${VITE_API_BASE_URL}/notifications`);
                const notificationData = notificationResponse.data.data;
                setNotification(notificationData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, [])
    return (
        <div>
            {
                notification.map((notifications,index) => (
                    <div key={index}>
                        <h4>{notifications.title}</h4>
                        <h2>{notifications.message}</h2>

                    </div>
                ))
            }
        </div>
    )
}

export default Notification