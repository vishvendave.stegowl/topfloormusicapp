import axios from 'axios';
import homeAPI from '../services/homeServices'
import React, { useEffect, useState } from 'react'
import { VITE_API_BASE_URL } from '../api/env';
import { Link } from 'react-router-dom';

function HomeSlider() {
    const [banners, setBanners] = useState([]);

    useEffect(() => {
        homeAPI.getHomeData().then((res)=>{
console.log('✌️res --->', res);
            const bannerData=res.homeslider_data;
             
            setBanners(bannerData);
        })
        // const fetchData = async () => {
        //     try {
        //         const bannerResponse = await axios.get(`${VITE_API_BASE_URL}/cms`);
        //         const bannerData = bannerResponse.data.homeslider_data;
        //         setBanners(bannerData);
        //     } catch (error) {
        //         console.error('Error fetching data:', error);
        //     }
        // };
        // fetchData();
    }, []);

    if (!banners || banners.length === 0) {
        return (
            <div className="d-flex justify-content-center mt-5">
                <div className="spinner-border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        );
    }
    return (
        <div>
            <div className="container mt-5">
                <div id="bannerCarousel" className="carousel slide" data-bs-ride="carousel" data-bs-interval="3000">
                    <div className="carousel-inner">
                        {banners.map((banner, index) => (
                            <div key={`banner-${index}`} className={`carousel-item ${index === 0 ? 'active' : ''}`}>
                                <Link to={banner.url} target="_blank" rel="noopener noreferrer">
                                    <img src={banner.image} className="d-block w-100" alt={`Banner ${index}`} />
                                </Link>
                            </div>
                        ))}
                    </div>
                    <button className="carousel-control-prev " type="button" data-bs-target="#bannerCarousel" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#bannerCarousel" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>
    )
}

export default HomeSlider
