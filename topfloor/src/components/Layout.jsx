import React from 'react';
import { useSelector } from 'react-redux';
import MusicPlayer from './MusicPlayer';


const Layout = ({ children }) => {
  const selectedSong = useSelector(state => state.music.selectedSongIndex);

  return (
    <div>
      {children}
      {selectedSong !== null && <MusicPlayer />}
    </div>
  );
};

export default Layout;

