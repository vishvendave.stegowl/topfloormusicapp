import React, { useState, useEffect } from 'react';

const ToastNotification = ({ message, duration = 3000, position = 'top-right', onClose }) => {
  const [showToast, setShowToast] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowToast(false);
      if (onClose) onClose();
    }, duration);

    return () => clearTimeout(timer);
  }, []);

  return (
    <>
      {showToast && (
        <div
          style={{
            position: 'fixed',
            top: position.includes('top') ? '20px' : 'unset',
            bottom: position.includes('bottom') ? '20px' : 'unset',
            right: position.includes('right') ? '20px' : 'unset',
            left: position.includes('left') ? '20px' : 'unset',
            zIndex: '9999'
          }}
        >
          <div
            className={`toast ${showToast ? 'show' : ''}`}
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
          >
            <div className="toast-header">
              <strong className="me-auto">Notification</strong>
              <button
                type="button"
                className="btn-close"
                onClick={() => setShowToast(false)}
              ></button>
            </div>
            <div className="toast-body">
              {message}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ToastNotification;
