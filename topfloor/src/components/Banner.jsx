import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { VITE_API_BASE_URL } from '../api/env';
import BannerCarousel from './BannerCarousel';
// import '../css/embla.css'
import '../css/embla.css'
import bannerAPI from '../services/homeServices'


export default function Banner({ carouselId }) {
    const [banners, setBanners] = useState([]);

    const OPTIONS = {}
    const SLIDE_COUNT = 5
    const SLIDES = Array.from(Array(SLIDE_COUNT).keys())

    useEffect(() => {
        bannerAPI.getHomeData().then((res)=>{
            const bannerData = res.sponsorbanner_data;
                setBanners(bannerData);
        })

        // const fetchData = async () => {
        //     try {
        //         const bannerResponse = await axios.get(`${VITE_API_BASE_URL}/cms`);
        //         const bannerData = bannerResponse.data.sponsorbanner_data;
        //         setBanners(bannerData);
        //     } catch (error) {
        //         console.error('Error fetching data:', error);
        //     }
        // };
        // fetchData();
    }, []);

    if (!banners || banners.length === 0) {
        return (
            <div className="d-flex justify-content-center mt-5">
                <div className="spinner-border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        );
    }

    return (
        <div className="container mt-5">
            <div id={`bannerCarousel-${carouselId}`} className="carousel slide " data-bs-ride="carousel" data-bs-interval="3000">
             {/* <EmblaCarousel slides={SLIDES} options={OPTIONS} /> */}
             {banners && (
                 <BannerCarousel slides={banners} options={OPTIONS}/>
             )}
            
            {/* <div id={`bannerCarousel-${carouselId}`} className="carousel slide" data-bs-ride="carousel" data-bs-interval="3000">
                <div className="carousel-inner">
                    {banners.map((banner, index) => (
                        <div key={`banner-${carouselId}-${index}`} className={`carousel-item ${index === 0 ? 'active' : ''}`}>
                            <Link to={banner.url} target="_blank" rel="noopener noreferrer">
                                <img src={banner.image} className="d-block w-100" alt={`Banner ${index}`} />
                            </Link>
                        </div>
                    ))}
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target={`#bannerCarousel-${carouselId}`} data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target={`#bannerCarousel-${carouselId}`} data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div> */}
        </div>
        </div>
        
    );
}
