import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Navbar from '../components/Navbar';
import { VITE_API_BASE_URL } from '../api/env';
import contactAPI from '../services/homeServices'

function Information(props) {
  const [contactInfo, setContactInfo] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const bannerUrl=props.bannerUrl

  useEffect(() => {
    contactAPI.getHomeData().then((res)=>{
console.log('✌️res of contact us  --->', res);
      setContactInfo(res.contactus_data[0]);
    }).catch((error)=>{
      setError('Error fetching contact info');
      console.error('Error fetching contact info:', error);
    })
    .finally(()=>{
      setLoading(false);
    })
    // const fetchContactInfo = async () => {
    //   try {
    //     const response = await axios.get(`${VITE_API_BASE_URL}/cms`);
    //     setContactInfo(response.data.contactus_data[0]);
    //   } catch (error) {
    //     setError('Error fetching contact info');
    //     console.error('Error fetching contact info:', error);
    //   } finally {
    //     setLoading(false);
    //   }
    // };

    // fetchContactInfo();
  }, []);

  return (
    <div className="info-container">
      <div className="white-container">
        <div className='info-image'>
          <h4>Contact Us</h4>
        {bannerUrl ? (
          <iframe src={bannerUrl} title="Sponsor Banner" ></iframe>
        ) : (
          <p>Loading...</p>
        )}
      </div>
      <div className="small-white-container">
        <div className="contact-details">
            <div className='contact-details-inner'>
                <h1 className="info-title">Info</h1>
                  <p><i className="fas fa-envelope"></i><a className='contact-link' href="mailto:booktopfloor@icloud.com"> {contactInfo?.email}</a></p>
                  <p><i className="fas fa-globe"></i> <a className='contact-link' href="topfloormusicapp.com">{contactInfo?.website}</a></p>
                  <p><i className="fas fa-phone"></i> <a className='contact-link' href="tel:917-682-2424">{contactInfo?.phone}</a></p>
                  <p><i className="fas fa-user"></i> {contactInfo?.address}</p>
            </div>
          <div className="red-small"> 
            
          </div>
      </div>
      
    </div>
      </div>
    </div>
//     <div className="info-container">
//     <div className="contact-left">
//         <h2>Contact Us</h2>
//         <div className="image-placeholder">
//             <img src="broken-image.png" alt="Image placeholder"/>
//         </div>
//     </div>
//     <div className="contact-right">
//         <div className="info-box">
//             <div className="red-tab"></div>
//             <div className="info">
//                 <p className="info-title">Info</p>
//                 <p><i className="fas fa-envelope"></i> booktopfloor@icloud.com</p>
//                 <p><i className="fas fa-globe"></i> topfloormusicapp.com</p>
//                 <p><i className="fas fa-phone"></i> Call or text 917-682-2424</p>
//                 <p><i className="fas fa-user"></i> World Wide Dj</p>
//             </div>
//         </div>
//     </div>
// </div>
    // <div>
    //   <center>
    //   {loading ? (
    //     <div className="spinner-border" role="status">
    //       <span className="visually-hidden">Loading...</span>
    //     </div>
    //   ) : error ? (
    //     <div>Error: {error}</div>
    //   ) : contactInfo && (
    //     <div>
    //       <h2>Contact Information</h2>
    //       <p>Email: {contactInfo.email}</p>
    //       <p>Website: {contactInfo.website}</p>
    //       <p>Phone: {contactInfo.phone}</p>
    //       <p>Address: {contactInfo.address}</p>
    //     </div>
    //   )}
    //   </center>
    // </div>
  );
}

export default Information;
