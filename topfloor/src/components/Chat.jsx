// import React, { useState, useEffect } from 'react';
// import axios from 'axios';

// function Chat() {
//   const [chatData, setChatData] = useState([]);
//   const [error, setError] = useState(null);
//   const [ws, setWs] = useState(null);
//   const [messageInput, setMessageInput] = useState('');
//   const [userId, setUserId] = useState('');

//   useEffect(() => {
//     fetchChatData();
//     setupWebSocket();
//     // Fetch user ID from local storage or any other source
//     const storedUserId = localStorage.getItem('userId');
//     if (storedUserId) {
//       // Use the stored user ID to set the state
//       setUserId(storedUserId);
//     }
//   }, []);

//   useEffect(() => {
//     if (ws) {
//       ws.onmessage = (event) => {
//         console.log('Received message:', event.data);
//         const newMessage = JSON.parse(event.data);
//         setChatData(prevChatData => [...prevChatData, newMessage]);
//       };

//       ws.onerror = (error) => {
//         console.error('WebSocket error:', error);
//         // Update state with error message
//         setError('WebSocket connection error');
//       };

//       ws.onclose = () => {
//         console.log('WebSocket disconnected');
       
//       };
//     }
//   }, [ws]);

//   const setupWebSocket = () => {
//     const newWs = new WebSocket('wss://topfloormusicapp.com/ws/livevideochat/');

//     newWs.onopen = () => {
//       console.log('WebSocket connected');
//     };

//     setWs(newWs);
//   };

//   const fetchChatData = async () => {
//     try {
//       const response = await axios.get('https://topfloormusicapp.com/auth/api/oldchatload');
//       setChatData(response.data.data);
//     } catch (error) {
//       setError('Error fetching chat data');
//       console.error('Error fetching chat data:', error);
//     }
//   };

//   const handleMessageChange = (event) => {
//     setMessageInput(event.target.value);
//   };

//   const sendMessage = () => {
//     if (!ws || ws.readyState !== WebSocket.OPEN) {
//       console.error('WebSocket connection not open');
//       return;
//     }

//     if (!messageInput.trim()) {
//       console.error('Message cannot be empty');
//       return;
//     }

//     const messageData = {
//       message: messageInput.trim(),
//       username: `user_${userId}`, 
//       room: 'livevideochat'
//     };

//     ws.send(JSON.stringify(messageData));
//     setMessageInput('');
//   };

//   return (
//     <div>
//       <h2>Chat</h2>
      
//       {error ? (
//         <div>Error: {error}</div>
//       ) : (
//         <div>
//           {chatData.map((message, index) => (
//             <div key={index}>
//               <p>{message.username}: {message.message}</p>
//               <p>{message.date_added}</p>
//             </div>
//           ))}
//         </div>
//       )}
//       <div>
//         <input type="text" value={messageInput} onChange={handleMessageChange} />
//         <button onClick={sendMessage}>Send</button>
//       </div>
//     </div>
//   );
// }

// export default Chat;


import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { VITE_API_BASE_URL } from '../api/env';
import '../css/Chat.css';

function Chat() {
  const [chatData, setChatData] = useState([]);
  const [error, setError] = useState(null);
  const [ws, setWs] = useState(null);
  const [messageInput, setMessageInput] = useState('');
  const [userId, setUserId] = useState('');

  useEffect(() => {
    fetchChatData();
    setupWebSocket();
    // Fetch user ID from local storage or any other source
    const storedUserId = localStorage.getItem('userId');
    if (storedUserId) {
      // Use the stored user ID to set the state
      setUserId(storedUserId);
    }
  }, []);

  useEffect(() => {
    if (ws) {
      ws.onmessage = (event) => {
        console.log('Received message:', event.data);
        const newMessage = JSON.parse(event.data);
        setChatData(prevChatData => [...prevChatData, newMessage]);
      };

      ws.onerror = (error) => {
        console.error('WebSocket error:', error);
        setError('WebSocket connection error');
      };

      ws.onclose = () => {
        console.log('WebSocket disconnected');
      };
    }
  }, [ws]);

  const setupWebSocket = () => {
    const newWs = new WebSocket('wss://topfloormusicapp.com/ws/livevideochat/');

    newWs.onopen = () => {
      console.log('WebSocket connected');
    };

    setWs(newWs);
  };

  const fetchChatData = async () => {
    try {
      const response = await axios.get(`${VITE_API_BASE_URL}/oldchatload`);
      setChatData(response.data.data);
    } catch (error) {
      setError('Error fetching chat data');
      console.error('Error fetching chat data:', error);
    }
  };

  const handleMessageChange = (event) => {
    setMessageInput(event.target.value);
  };

  const sendMessage = () => {
    if (!ws || ws.readyState !== WebSocket.OPEN) {
      console.error('WebSocket connection not open');
      return;
    }

    if (!messageInput.trim()) {
      console.error('Message cannot be empty');
      return;
    }

    const messageData = {
      message: messageInput.trim(),
      username: `user_${userId}`,
      room: 'livevideochat'
    };

    ws.send(JSON.stringify(messageData));
    setMessageInput('');
  };

  return (
    // <div className='chat-contain-inner'>
    //    {error ? (
    //   <div>Error: {error}</div>
    //     ) : (
    //    chatData.map((message, index) => (
    //   <div className="message" key={index}>
    //     <div className='chat-layout-container'>
    //       <div className="avatar">
    //         <img className='user-avatar' src="https://cdn.iconscout.com/icon/free/png-512/free-user-1045-252486.png?f=webp&w=256" alt="" />
    //         </div>
    //       <div className='user-info'>
    //       <div className="username">{message.username}</div>
    //         <div className="text">{message.message}</div>
    //         <div className="timestamp">{message.username} | {message.date_added}</div>
    //       </div>
    //     </div>
        

    //   </div>
    //  ))
    // )}

    // <div class="live-chat-input">
    //     <input type="text" placeholder='Type Something..' value={messageInput} onChange={handleMessageChange} />
    //      <button onClick={sendMessage}>Send</button>
    // </div>

    // </div>


    <div className="gradiant-custom">
          <div className="container-chats">
            <div className="scroll-container">

                       {error ? (
       <div>Error: {error}</div>
         ) : (
        chatData.map((message, index) => (
       <div className="message" key={index}>
         <div className='chat-layout-container'>
           <div className="avatar">
             <img className='user-avatar' src="https://cdn.iconscout.com/icon/free/png-512/free-user-1045-252486.png?f=webp&w=256" alt="" />
             </div>
           <div className='user-info'>
           <div className="username">{message.username}</div>
             <div className="text">{message.message}</div>
             <div className="timestamp">{message.username} | {message.date_added}</div>
           </div>
         </div>
        

       </div>
      ))
     )}
            </div>
             <div class="live-chat-input">
                <input type="text" placeholder='Type Something..' value={messageInput} onChange={handleMessageChange} />
                 <button onClick={sendMessage}>Send</button>
            </div>
                   

          </div>

    </div>

//     <div class="live-stream-chat-container">
//     <div class="live-stream-chat-messages">
    
//     {/* {error ? (
//         <div>Error: {error}</div>
//       ) :(
//         {chatData.map((message, index) => (
//         <div class="message">
//             <div class="avatar">user_119</div>
//             <div class="text">hii</div>
//             <div class="timestamp">06/05/2024 - 07:49:44</div>
//         </div>
//         ))
//       )} */}
//       {error ? (
//     <div>Error: {error}</div>
//         ) : (
//        chatData.map((message, index) => (
//       <div className="message" key={index}>
//         <div className="avatar">user_119</div>
//         <div className="text">{message.message}</div>
//         <div className="timestamp">{message.username} | {message.date_added}</div>
//       </div>
//     ))
//   )}

        

        
        
        
//     </div>
//     <div class="live-chat-input">
//         <input type="text" placeholder='Type Something..' value={messageInput} onChange={handleMessageChange} />
//          <button onClick={sendMessage}>Send</button>
//     </div>
// </div>


















    // <div className="chat-container">
    //   <h2> Global Chat</h2>

    //   {error ? (
    //     <div>Error: {error}</div>
    //   ) : (
    //     <div className="chat-messages">
    //       {chatData.map((message, index) => (
    //         <div key={index} className="chat-message">
    //           <p className="message-text">{message.message}</p>
    //           <p className="message-info">{message.username} | {message.date_added}</p>
    //         </div>
    //       ))}
    //     </div>
    //   )}
    //   <div className="chat-input">
    //     <input type="text" value={messageInput} onChange={handleMessageChange} />
    //     <button onClick={sendMessage}>Send</button>
    //   </div>
    // </div>
  );
}

export default Chat;

