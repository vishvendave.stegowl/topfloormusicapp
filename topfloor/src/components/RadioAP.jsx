import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { VITE_API_BASE_URL } from '../api/env';
import radioApAPI from '../services/homeServices'

export default function RadioAP() {
    const [radioap, setRadioAP] = useState([]);
    const [selectedRadio, setSelectedRadio] = useState(null);

    useEffect(() => {
        radioApAPI.fetchRadioApData().then((res)=>{
            const bannerData = res.data;
            setRadioAP(bannerData); 
        })
        // const fetchData = async () => {
        //     try {
        //         const redioResponse = await axios.get(`${VITE_API_BASE_URL}/program`);
        //         const bannerData = redioResponse.data.data;
        //         setRadioAP(bannerData);
        //     } catch (error) {
        //         console.error('Error fetching data:', error);
        //     }
        // };
        // fetchData();

    }, []);

    const handleRadioClick = (radio) => {
        setSelectedRadio(radio);
    };

    const handleCloseDetail = () => {
        setSelectedRadio(null);
    };

    return (
        <div >
            {selectedRadio && (
                // <div className="card detail-view position-fixed top-50 start-50 translate-middle">
                //     <button className="btn btn-danger" onClick={handleCloseDetail}>Close</button>
                //     <img src={selectedRadio.event_image} className="card-img-top" alt="Banner" height={150} width={150} />
                //     <h3>{selectedRadio.event_name}</h3>
                //     <p>{selectedRadio.event_days}</p>
                //     <p>{selectedRadio.event_start_time} - {selectedRadio.event_end_time}</p>
                //     <p className="card-text">{selectedRadio.event_description}</p>
                // </div>
                <div className='radio-main-modal'>

                    <div className='radio-modal-content'>
                        <span className='close-button' onClick={handleCloseDetail}>X</span>
                        <div className="modal-image">
                            <img src={selectedRadio.event_image} className="card-img-top" alt="Banner" />
                        </div>
                        <div className="modal-details">

                            <h3 className='radio-modal-text-r headd' >{selectedRadio.event_name}</h3>
                            <p className='radio-modal-text '>{selectedRadio.event_days}</p>
                            <p className='radio-modal-text small'>{selectedRadio.event_start_time} - {selectedRadio.event_end_time}</p>
                            <p className='radio-modal-text-r'>{selectedRadio.event_description}</p>
                            {/* <button className="btn btn-danger" onClick={handleCloseDetail}>Close</button> */}

                        </div>
                    </div>

                </div>
            )}
            {/* <div className="row">
                {radioap.map((radio, index) => (
                    <div key={`banner-${index}`} className="col-md-4 mb-4">
                        <div className="card" style={{ cursor: 'pointer' }} onClick={() => handleRadioClick(radio)}>
                            <img src={radio.event_image} className="card-img-top" alt="Banner" height={150} width={150} />
                            <div className="card-body">
                                <h3 className="card-title">{radio.event_name}</h3>
                                <p className="card-text">{radio.event_days}</p>
                                <p className="card-text">{radio.event_start_time} - {radio.event_end_time}</p>
                            </div>
                        </div>
                    </div>
                ))}
            </div> */}

            {/* showtime card  */}

            <div className="radio-main-card-container">

                <div className="radio-card-container">
                    {radioap?.map((radio, index) => (
                        <div className="radio-card" key={`banner-${index}`} onClick={() => handleRadioClick(radio)}>

                            <img src={radio.event_image} className="card-img-top" alt="Banner" />
                            <div className='showtime-text-container'>
                                <h6 className="radio-card-title">{radio.event_name}</h6>
                                <p className="white p-1">{radio.event_days}</p>
                                {/* <p className="white small">{radio.event_start_time} - {radio.event_end_time}</p> */}
                                <span>{radio.event_start_time} - {radio.event_end_time}</span>

                            </div>

                        </div>
                    ))}
                </div>
            </div>

        </div>
    );
}
