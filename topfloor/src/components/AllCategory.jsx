import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { VITE_API_BASE_URL } from '../api/env';
import allCatAPI from '../services/homeServices'

const AllCategory = () => {
    const { id } = useParams();
    const [categoryData, setCategoryData] = useState([]);
    const [error, setError] = useState(null);
    const navigate = useNavigate();

    useEffect(() => {

        window.scrollTo(0, 0);

    }, []);

    useEffect(() => {
        allCatAPI.fetchAllCategoryData(id).then((res)=>{
            setCategoryData(res.data);
        })
        .catch((error)=>{
            console.error('Error fetching category data:', error);
            setError('An error occurred while fetching the category data.');
        })
       
       
        // const fetchData = async () => {
        //     try {
        //         const userId = localStorage.getItem('userId');
        //         const token = localStorage.getItem('token');
                
        //         if (!userId || !token) {
        //           navigate('/');
        //         }
        //         const response = await axios.get(`${VITE_API_BASE_URL}/songcategory/${id}`);
        //         setCategoryData(response.data.data);
        //     } catch (error) {
        //         console.error('Error fetching category data:', error);
        //         setError('An error occurred while fetching the category data.');
        //     }
        // };
        // fetchData();
    }, [id]);

    return (
        <div className='container'>
            
            {/* <div className='container'>
                <h1>Category Details</h1>
                {error ? (
                    <p>{error}</p>
                ) : (
                    <div className="row">
                        {categoryData.map((category) => (
                            <div key={category.songcategory_id} className="col-md-4 mb-4">
                                <div className="card">
                                    <div className="card-body">
                                        <h3 className="card-title">{category.name}</h3>
                                        <Link to={`/categorymusic/${category.songcategory_id}`}>
                                            <img src={category.image} className="card-img-top" alt="category" height={200} width={150} />
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                )}
            </div> */}

          
<h3 className='music-red-headings' >Music Collection</h3>
    <div className="main-card-container">
        
    
      <div className='music-card-container'>
        
        {error ? (
          <p>{error}</p>
        ) : (
            categoryData.map((category) => (
            
              <div className="music-card">
                <div className="left">
                <Link to={`/categorymusic/${category.songcategory_id}`}>
                                            <img src={category.image} className="card-img-top" alt="category"  />
                                        </Link>
                </div>
                
                <div className="right">
                <Link to={`/categorymusic/${category.songcategory_id}`} style={{textDecoration:'none',color:'white'}}>
                  <h4 className='linkText'>{category.name}</h4>
                  </Link>
                </div>
              </div>
          
          ))
        )}

                    


                </div>

            </div>
            
        </div>
    );
}

export default AllCategory;

