
import { createSlice } from '@reduxjs/toolkit';

const musicSlice = createSlice({
    name: 'music',
    initialState: {
        songs: [], // Array of songs
        selectedSongIndex: null, // Index of the selected song in the songs array
    },
    reducers: {
        setSelectedSong: (state, action) => {
            state.selectedSongIndex = action.payload;
        },
        selectNextSong: state => {
            if (state.selectedSongIndex !== null && state.selectedSongIndex > 0) {
                state.selectedSongIndex -= 1;
            }
            // if (state.selectedSongIndex !== null && state.selectedSongIndex < state.songs.length - 1) {
            //     state.selectedSongIndex++;
            // }
        },
        
        selectPreviousSong(state) {
            if (state.selectedSongIndex !== null && state.selectedSongIndex > 0) {
                state.selectedSongIndex += 1;
            }
            // if (state.selectedSongIndex !== null && state.selectedSongIndex < state.songs.length - 1) {
            //     state.selectedSongIndex++;
            // }
        },
        setSongs: (state, action) => {
            state.songs = action.payload;
        },
    },
});

export const { selectNextSong, selectPreviousSong, setSelectedSong, setSongs } = musicSlice.actions;

export default musicSlice.reducer;