import { configureStore } from '@reduxjs/toolkit';
import musicReducer from './MusicSlice';

const Store = configureStore({
    reducer: {
        music: musicReducer,
    },
});
export default Store