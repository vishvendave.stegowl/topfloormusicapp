// import './App.css'

// import { BrowserRouter as Router, Routes, Route, useParams, useLocation } from 'react-router-dom';
// import Banner from './components/Banner'

// import Categorymusic from './components/Categorymusic'
// import Home from './pages/Home';
// import Entry from './pages/Entry';
// import MyFavorites from './pages/MyFavorites';
// import Navbar from './components/Navbar';
// import Radio from './pages/Radio';
// import Showtime from './pages/Showtime';
// import LiveStream from './pages/LiveStream';
// import Info from './pages/Info';
// import Events from './pages/Events';
// import Music from './pages/Music';
// import MyPlayList from './pages/MyPlayList';
// import PlaylistSongsPage from './components/PlaylistSongPage';
// import Search from './pages/Search';
// import AllCategory from './components/AllCategory';
// import Footer from './components/Footer';

// function App() {
//   return (
//     <Router>
//       <AppRoutes />
//     </Router>
//   );
// }

// function AppRoutes() {
//   const location = useLocation();
//   const isEntryPage = location.pathname === '/';

//   return (
//     <>
//       {!isEntryPage && <Navbar />}
//       {!isEntryPage && <Banner carouselId="1"/> }
//       <Routes>
//       <Route path="/" element={<Entry/>} />
//               <Route path="/banner" element={<Banner/>} />
//               <Route path="/home" element={<Home/>} />
//               <Route path="categorymusic/:id" element={<Categorymusic/>} />
//               <Route path="/music" element={<Music/>} />
//                   <Route path="/allcategory/:id" element={<AllCategory/>} />
//               <Route path="/radio" element={<Radio/>} />
//               <Route path="/livestream" element={<LiveStream/>} />
//               <Route path="/events" element={<Events/>} />
//               <Route path="/showtime" element={<Showtime/>} />
//               <Route path="/myplaylist" element={<MyPlayList/>} />
//                   <Route path="/playlist/:playlistId" element={<PlaylistSongsPage/>} />
//               {/* {/ <Route path="/wishlist" element={<Wishlist/>} /> /} */}
//               <Route path="/favorites" element={<MyFavorites/>} />
//               <Route path="/info" element={<Info/>} />
//               <Route path="/search" element={<Search/>} />
//         {/* Add other routes here */}
//       </Routes>

//       {!isEntryPage && <Banner carouselId="2"/>}
//       {!isEntryPage && <Footer />}
//     </>
//   );
// }

// export default App;

import "./App.css";

import {
  BrowserRouter as Router,
  Routes,
  Route,
  useParams,
  useLocation,
} from "react-router-dom";
import Banner from "./components/Banner";
import { Provider } from "react-redux";
import Store from "./redux/Store";
import Categorymusic from "./components/Categorymusic";
import Home from "./pages/Home";
import Entry from "./pages/Entry";
import MyFavorites from "./pages/MyFavorites";
import Navbar from "./components/Navbar";
import Radio from "./pages/Radio";
import Showtime from "./pages/Showtime";
import LiveStream from "./pages/LiveStream";
import Info from "./pages/Info";
import Events from "./pages/Events";
import Music from "./pages/Music";
import MyPlayList from "./pages/MyPlayList";
import PlaylistSongsPage from "./components/PlaylistSongPage";
import Search from "./pages/Search";
import AllCategory from "./components/AllCategory";
import Footer from "./components/Footer";
import Layout from "./components/Layout";

// function App() {
//   return (
//     <Router>
//       <AppRoutes />
//     </Router>
//   );
// }
function App() {
  return (
    <div>
    <Router>
      <AppRoutes />
    </Router>
    </div>
  );
}

function AppRoutes() {
  const location = useLocation();
  const isEntryPage = location.pathname === "/";

  return (
    <>
      <Provider store={Store}>
        {!isEntryPage && <Navbar />}
        {!isEntryPage && <Banner carouselId="1" />}
        <Layout>
          <Routes>
            <Route path="/" element={<Entry />} />
            <Route path="/home" element={<Home />} />
            <Route path="categorymusic/:id" element={<Categorymusic />} />
            <Route path="/music" element={<Music />} />
            <Route path="/allcategory/:id" element={<AllCategory />} />
            <Route path="/radio" element={<Radio />} />
            <Route path="/livestream" element={<LiveStream />} />
            <Route path="/events" element={<Events />} />
            <Route path="/showtime" element={<Showtime />} />
            <Route path="/myplaylist" element={<MyPlayList />} />
            <Route
              path="/playlist/:playlistId"
              element={<PlaylistSongsPage />}
            />
            <Route path="/favorites" element={<MyFavorites />} />
            <Route path="/info" element={<Info />} />
            <Route path="/search" element={<Search />} />
          </Routes>
        </Layout>
        {/* {/ {!abc && <MusicPlayer />} /} */}
        {!isEntryPage && <Banner carouselId="2" />}
        {!isEntryPage && <Footer />}
      </Provider>
    </>
  );
}

export default App;
